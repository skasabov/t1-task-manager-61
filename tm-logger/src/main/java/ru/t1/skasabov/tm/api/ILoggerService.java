package ru.t1.skasabov.tm.api;

import org.jetbrains.annotations.NotNull;

public interface ILoggerService {

    void log(@NotNull String json);

}
