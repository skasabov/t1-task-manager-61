package ru.t1.skasabov.tm.taskmanager.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.t1.skasabov.tm.api.service.dto.IProjectDTOService;
import ru.t1.skasabov.tm.api.service.dto.IProjectTaskDTOService;
import ru.t1.skasabov.tm.api.service.dto.ITaskDTOService;
import ru.t1.skasabov.tm.api.service.dto.IUserDTOService;
import ru.t1.skasabov.tm.dto.model.ProjectDTO;
import ru.t1.skasabov.tm.dto.model.TaskDTO;
import ru.t1.skasabov.tm.enumerated.Sort;
import ru.t1.skasabov.tm.enumerated.Status;
import ru.t1.skasabov.tm.exception.entity.ModelEmptyException;
import ru.t1.skasabov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.skasabov.tm.exception.entity.TaskNotFoundException;
import ru.t1.skasabov.tm.exception.field.*;
import ru.t1.skasabov.tm.taskmanager.AbstractTest;

import java.util.*;
import java.util.stream.Collectors;

public class TaskDTOServiceTest extends AbstractTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static String USER_ID_ONE;

    @NotNull
    private static String USER_ID_TWO;

    @NotNull
    private ProjectDTO projectOne;

    @NotNull
    private ProjectDTO projectTwo;

    @NotNull
    private ITaskDTOService taskService;

    @NotNull
    private IUserDTOService userService;

    @NotNull
    private IProjectTaskDTOService projectTaskService;

    @Before
    public void initTest() {
        final long currentTime = System.currentTimeMillis();
        @NotNull final IProjectDTOService projectService = context.getBean(IProjectDTOService.class);
        userService = context.getBean(IUserDTOService.class);
        taskService = context.getBean(ITaskDTOService.class);
        projectTaskService = context.getBean(IProjectTaskDTOService.class);
        USER_ID_ONE = userService.create("user_one", "user_one").getId();
        USER_ID_TWO = userService.create("user_two", "user_two").getId();
        projectOne = projectService.create(USER_ID_ONE, "project_one");
        projectTwo = projectService.create(USER_ID_ONE, "project_two");
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final TaskDTO task = new TaskDTO();
            task.setName("Test " + i);
            task.setCreated(new Date(currentTime + i * 1000));
            if (i < 4) task.setStatus(Status.COMPLETED);
            else if (i < 7) task.setStatus(Status.IN_PROGRESS);
            else task.setStatus(Status.NOT_STARTED);
            if (i <= 5) {
                task.setUserId(USER_ID_ONE);
                task.setProjectId(projectOne.getId());
            } else {
                task.setUserId(USER_ID_TWO);
                task.setProjectId(projectTwo.getId());
            }
            taskService.add(task);
        }
    }

    @Test(expected = UserIdEmptyException.class)
    public void testCreateForEmptyUser() {
        taskService.create("", "test", "");
    }

    @Test(expected = UserIdEmptyException.class)
    public void testCreateForNullUser() {
        taskService.create(null, "test", "");
    }

    @Test(expected = NameEmptyException.class)
    public void testCreateEmptyName() {
        taskService.create(USER_ID_ONE, "", "");
    }

    @Test(expected = NameEmptyException.class)
    public void testCreateNullName() {
        taskService.create(USER_ID_ONE, null, "");
    }

    @Test
    public void testCreateName() {
        final long expectedNumberOfEntries = taskService.getSize() + 1;
        @NotNull final String name = "Test Task";
        @NotNull final TaskDTO actualTask = taskService.create(USER_ID_ONE, name, "");
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize());
        Assert.assertEquals(USER_ID_ONE, actualTask.getUserId());
        Assert.assertEquals(name, actualTask.getName());
        Assert.assertEquals("", actualTask.getDescription());
    }

    @Test
    public void testCreateDescription() {
        final long expectedNumberOfEntries = taskService.getSize() + 1;
        @NotNull final String name = "Test Task";
        @NotNull final String description = "Test Description";
        @NotNull final TaskDTO actualTask = taskService.create(USER_ID_TWO, name, description);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize());
        Assert.assertEquals(USER_ID_TWO, actualTask.getUserId());
        Assert.assertEquals(name, actualTask.getName());
        Assert.assertEquals(description, actualTask.getDescription());
    }

    @Test
    public void createTask() {
        final long expectedNumberOfEntries = taskService.getSize() + 1;
        @NotNull final String name = "Test Task";
        @NotNull final String description = "Test Description";
        @NotNull final Date dateBegin = new Date();
        @NotNull final Date dateEnd = new Date();
        @NotNull final TaskDTO actualTask = taskService.create(USER_ID_ONE, name, description, dateBegin, dateEnd);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize());
        Assert.assertEquals(USER_ID_ONE, actualTask.getUserId());
        Assert.assertEquals(name, actualTask.getName());
        Assert.assertEquals(description, actualTask.getDescription());
        Assert.assertEquals(dateBegin, actualTask.getDateBegin());
        Assert.assertEquals(dateEnd, actualTask.getDateEnd());
    }

    @Test
    public void testUpdateById() {
        @NotNull final String id = taskService.findAll(USER_ID_TWO).get(0).getId();
        @NotNull final String name = "Test Task One";
        @NotNull final String description = "Test Description One";
        @NotNull final TaskDTO task = taskService.updateById(USER_ID_TWO, id, name, description);
        Assert.assertEquals(USER_ID_TWO, task.getUserId());
        Assert.assertEquals(name, task.getName());
        Assert.assertEquals(description, task.getDescription());
    }

    @Test(expected = IdEmptyException.class)
    public void testUpdateByEmptyId() {
        @NotNull final String name = "Test Task One";
        @NotNull final String description = "Test Description One";
        taskService.updateById(USER_ID_TWO, "", name, description);
    }

    @Test(expected = IdEmptyException.class)
    public void testUpdateByNullId() {
        @NotNull final String name = "Test Project One";
        @NotNull final String description = "Test Description One";
        taskService.updateById(USER_ID_TWO, null, name, description);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testUpdateByIdForEmptyUser() {
        @NotNull final String id = taskService.findAll(USER_ID_TWO).get(0).getId();
        @NotNull final String name = "Test Task One";
        @NotNull final String description = "Test Description One";
        taskService.updateById("", id, name, description);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testUpdateByIdForNullUser() {
        @NotNull final String id = taskService.findAll(USER_ID_TWO).get(0).getId();
        @NotNull final String name = "Test Project One";
        @NotNull final String description = "Test Description One";
        taskService.updateById(null, id, name, description);
    }

    @Test(expected = TaskNotFoundException.class)
    public void testUpdateByIdForIncorrectUser() {
        @NotNull final String id = taskService.findAll(USER_ID_TWO).get(0).getId();
        @NotNull final String name = "Test Project One";
        @NotNull final String description = "Test Description One";
        taskService.updateById("some_id", id, name, description);
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateByIdWithEmptyName() {
        @NotNull final String id = taskService.findAll(USER_ID_TWO).get(0).getId();
        @NotNull final String description = "Test Description One";
        taskService.updateById(USER_ID_TWO, id, "", description);
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateByIdWithNullName() {
        @NotNull final String id = taskService.findAll(USER_ID_TWO).get(0).getId();
        @NotNull final String description = "Test Description One";
        taskService.updateById(USER_ID_TWO, id, null, description);
    }

    @Test(expected = TaskNotFoundException.class)
    public void testUpdateByIdTaskNotFound() {
        @NotNull final String id = UUID.randomUUID().toString();
        @NotNull final String name = "Test Task One";
        @NotNull final String description = "Test Description One";
        taskService.updateById(USER_ID_TWO, id, name, description);
    }

    @Test
    public void testUpdateByIndex() {
        @NotNull final String name = "Test Task One";
        @NotNull final String description = "Test Description One";
        @NotNull final TaskDTO task = taskService.updateByIndex(USER_ID_ONE, 1, name, description);
        Assert.assertEquals(USER_ID_ONE, task.getUserId());
        Assert.assertEquals(name, task.getName());
        Assert.assertEquals(description, task.getDescription());
    }

    @Test(expected = IndexIncorrectException.class)
    public void testUpdateByEmptyIndex() {
        @NotNull final String name = "Test Task One";
        @NotNull final String description = "Test Description One";
        taskService.updateByIndex(USER_ID_ONE, null, name, description);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testUpdateByIndexForEmptyUser() {
        @NotNull final String name = "Test Task One";
        @NotNull final String description = "Test Description One";
        taskService.updateByIndex("", 1, name, description);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testUpdateByIndexForNullUser() {
        @NotNull final String name = "Test Project One";
        @NotNull final String description = "Test Description One";
        taskService.updateByIndex(null, 1, name, description);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testUpdateByIndexForIncorrectUser() {
        @NotNull final String name = "Test Project One";
        @NotNull final String description = "Test Description One";
        taskService.updateByIndex("some_id", 1, name, description);
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateByIndexWithEmptyName() {
        @NotNull final String description = "Test Description One";
        taskService.updateByIndex(USER_ID_ONE, 1, "", description);
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateByIndexWithNullName() {
        @NotNull final String description = "Test Description One";
        taskService.updateByIndex(USER_ID_ONE, 1, null, description);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testUpdateByNegativeIndex() {
        @NotNull final String name = "Test Task One";
        @NotNull final String description = "Test Description One";
        taskService.updateByIndex(USER_ID_ONE, -2, name, description);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testUpdateByIncorrectIndex() {
        @NotNull final String name = "Test Task One";
        @NotNull final String description = "Test Description One";
        taskService.updateByIndex(USER_ID_ONE, 5, name, description);
    }

    @Test
    public void testChangeTaskStatusById() {
        @NotNull final String id = taskService.findAll(USER_ID_TWO).get(0).getId();
        @NotNull final Status status = Status.NOT_STARTED;
        @NotNull final TaskDTO task = taskService.changeTaskStatusById(USER_ID_TWO, id, status);
        Assert.assertEquals(USER_ID_TWO, task.getUserId());
        Assert.assertEquals(status, task.getStatus());
    }

    @Test(expected = IdEmptyException.class)
    public void testChangeTaskStatusByEmptyId() {
        @NotNull final Status status = Status.NOT_STARTED;
        taskService.changeTaskStatusById(USER_ID_TWO, "", status);
    }

    @Test(expected = IdEmptyException.class)
    public void testChangeProjectStatusByNullId() {
        @NotNull final Status status = Status.NOT_STARTED;
        taskService.changeTaskStatusById(USER_ID_TWO, null, status);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testChangeTaskStatusByIdForEmptyUser() {
        @NotNull final String id = taskService.findAll(USER_ID_TWO).get(0).getId();
        @NotNull final Status status = Status.NOT_STARTED;
        taskService.changeTaskStatusById("", id, status);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testChangeProjectStatusByIdForNullUser() {
        @NotNull final String id = taskService.findAll(USER_ID_TWO).get(0).getId();
        @NotNull final Status status = Status.NOT_STARTED;
        taskService.changeTaskStatusById(null, id, status);
    }

    @Test(expected = TaskNotFoundException.class)
    public void testChangeProjectStatusByIdForIncorrectUser() {
        @NotNull final String id = taskService.findAll(USER_ID_TWO).get(0).getId();
        @NotNull final Status status = Status.NOT_STARTED;
        taskService.changeTaskStatusById("some_id", id, status);
    }

    @Test(expected = StatusEmptyException.class)
    public void testChangeTaskStatusByIdWithEmptyStatus() {
        @NotNull final String id = taskService.findAll(USER_ID_TWO).get(0).getId();
        taskService.changeTaskStatusById(USER_ID_TWO, id, Status.toStatus(null));
    }

    @Test(expected = StatusIncorrectException.class)
    public void testChangeTaskStatusByIdWithIncorrectStatus() {
        @NotNull final String id = taskService.findAll(USER_ID_TWO).get(0).getId();
        taskService.changeTaskStatusById(USER_ID_TWO, id, Status.toStatus("123"));
    }

    @Test(expected = TaskNotFoundException.class)
    public void testChangeTaskStatusByIdTaskNotFound() {
        @NotNull final String id = UUID.randomUUID().toString();
        @NotNull final Status status = Status.NOT_STARTED;
        taskService.changeTaskStatusById(USER_ID_TWO, id, status);
    }

    @Test
    public void testChangeTaskStatusByIndex() {
        @NotNull final Status status = Status.NOT_STARTED;
        @NotNull final TaskDTO task = taskService.changeTaskStatusByIndex(USER_ID_ONE, 1, status);
        Assert.assertEquals(USER_ID_ONE, task.getUserId());
        Assert.assertEquals(status, task.getStatus());
    }

    @Test(expected = IndexIncorrectException.class)
    public void testChangeTaskStatusByEmptyIndex() {
        @NotNull final Status status = Status.NOT_STARTED;
        taskService.changeTaskStatusByIndex(USER_ID_ONE, null, status);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testChangeTaskStatusByIndexForEmptyUser() {
        @NotNull final Status status = Status.NOT_STARTED;
        taskService.changeTaskStatusByIndex("", 1, status);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testChangeProjectStatusByIndexForNullUser() {
        @NotNull final Status status = Status.NOT_STARTED;
        taskService.changeTaskStatusByIndex(null, 1, status);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testChangeProjectStatusByIndexForIncorrectUser() {
        @NotNull final Status status = Status.NOT_STARTED;
        taskService.changeTaskStatusByIndex("some_id", 1, status);
    }

    @Test(expected = StatusEmptyException.class)
    public void testChangeTaskStatusByIndexWithEmptyStatus() {
        taskService.changeTaskStatusByIndex(USER_ID_ONE, 1, Status.toStatus(null));
    }

    @Test(expected = StatusIncorrectException.class)
    public void testChangeTaskStatusByIndexWithIncorrectStatus() {
        taskService.changeTaskStatusByIndex(USER_ID_ONE, 1, Status.toStatus("123"));
    }

    @Test(expected = IndexIncorrectException.class)
    public void testChangeTaskStatusByIncorrectIndex() {
        @NotNull final Status status = Status.NOT_STARTED;
        taskService.changeTaskStatusByIndex(USER_ID_ONE, 5, status);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testChangeTaskStatusByNegativeIndex() {
        @NotNull final Status status = Status.NOT_STARTED;
        taskService.changeTaskStatusByIndex(USER_ID_ONE, -2, status);
    }

    @Test
    public void testAdd() {
        final long expectedNumberOfEntries = taskService.getSize() + 1;
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName("Test Task");
        task.setUserId(USER_ID_TWO);
        taskService.add(task);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize());
    }

    @Test(expected = ModelEmptyException.class)
    public void testAddNull() {
        taskService.add(null);
    }

    @Test
    public void testAddAll() {
        final long expectedNumberOfEntries = taskService.getSize() + 4;
        @NotNull final List<TaskDTO> actualTasks = new ArrayList<>();
        for (int i = 1; i <= 4; i++) {
            @NotNull final TaskDTO task = new TaskDTO();
            task.setName("Test Task " + i);
            task.setUserId(USER_ID_ONE);
            actualTasks.add(task);
        }
        taskService.addAll(actualTasks);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize());
    }

    @Test
    public void testAddAllNull() {
        final long expectedNumberOfEntries = taskService.getSize();
        taskService.addAll(null);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize());
    }

    @Test
    public void testAddAllEmpty() {
        final long expectedNumberOfEntries = taskService.getSize();
        taskService.addAll(Collections.emptyList());
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize());
    }

    @Test
    public void testSet() {
        @NotNull final List<TaskDTO> actualTasks = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final TaskDTO task = new TaskDTO();
            task.setName("Test Task " + i);
            task.setUserId(USER_ID_TWO);
            actualTasks.add(task);
        }
        taskService.set(actualTasks);
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskService.getSize());
    }

    @Test
    public void testSetNull() {
        final long expectedNumberOfEntries = taskService.getSize();
        taskService.set(null);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize());
    }

    @Test
    public void testSetEmpty() {
        taskService.set(Collections.emptyList());
        Assert.assertEquals(0, taskService.getSize());
    }

    @Test
    public void testClearAll() {
        taskService.removeAll();
        Assert.assertEquals(0, taskService.getSize());
    }

    @Test
    public void testClearAllForUser() {
        final long expectedNumberOfEntries = taskService.getSize() - NUMBER_OF_ENTRIES / 2;
        taskService.removeAll(USER_ID_ONE);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize());
    }

    @Test(expected = UserIdEmptyException.class)
    public void testClearAllForEmptyUser() {
        taskService.removeAll("");
    }

    @Test(expected = UserIdEmptyException.class)
    public void testClearAllForNullUser() {
        taskService.removeAll((String) null);
    }

    @Test
    public void testClearAllForIncorrectUser() {
        taskService.removeAll("some_id");
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskService.getSize());
    }

    @Test
    public void testClear() {
        final long expectedNumberOfEntries = taskService.getSize() - NUMBER_OF_ENTRIES / 2;
        @NotNull final List<TaskDTO> taskList = taskService.findAll(USER_ID_TWO);
        taskService.removeAll(taskList);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize());
    }

    @Test
    public void testClearNull() {
        taskService.removeAll((List<TaskDTO>) null);
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskService.getSize());
    }

    @Test
    public void testClearEmpty() {
        taskService.removeAll(Collections.emptyList());
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskService.getSize());
    }

    @Test
    public void testFindAll() {
        @NotNull final List<TaskDTO> taskList = taskService.findAll();
        Assert.assertEquals(taskList.size(), taskService.getSize());
    }

    @Test
    public void testFindAllWithNameComparator() {
        @NotNull final Sort sortType = Sort.BY_NAME;
        @NotNull final List<TaskDTO> taskSortList = taskService.findAll(sortType);
        @NotNull final List<TaskDTO> actualTasks = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final TaskDTO task = new TaskDTO();
            task.setName("Test " + i);
            actualTasks.add(task);
        }
        actualTasks.add(1, actualTasks.get(NUMBER_OF_ENTRIES - 1));
        actualTasks.remove(NUMBER_OF_ENTRIES);
        Assert.assertEquals(actualTasks, taskSortList);
    }

    @Test
    public void testFindAllWithCreatedComparator() {
        @NotNull final Sort sortType = Sort.BY_CREATED;
        @NotNull final List<TaskDTO> taskSortList = taskService.findAll(sortType);
        @NotNull final List<TaskDTO> actualTasks = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final TaskDTO task = new TaskDTO();
            task.setName("Test " + i);
            actualTasks.add(task);
        }
        Assert.assertEquals(actualTasks, taskSortList);
    }

    @Test
    public void testFindAllWithStatusComparator() {
        @NotNull final Sort sortType = Sort.BY_STATUS;
        @NotNull final List<Status> statusList = taskService.findAll(sortType)
                .stream().map(TaskDTO::getStatus).collect(Collectors.toList());
        @NotNull final List<Status> actualStatuses = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final TaskDTO task = new TaskDTO();
            task.setName("Test " + i);
            if (i < 4) actualStatuses.add(Status.COMPLETED);
            else if (i < 7) actualStatuses.add(Status.IN_PROGRESS);
            else actualStatuses.add(Status.NOT_STARTED);
        }
        Assert.assertEquals(actualStatuses, statusList);
    }

    @Test
    public void testFindAllWithEmptyComparator() {
        @NotNull final List<TaskDTO> taskList = taskService.findAll();
        @NotNull final List<TaskDTO> taskSortList = taskService.findAll((Sort) null);
        Assert.assertEquals(taskList, taskSortList);
    }

    @Test(expected = SortIncorrectException.class)
    public void testFindAllWithIncorrectComparator() {
        taskService.findAll(Sort.toSort("123"));
    }

    @Test
    public void testFindAllForUser() {
        @NotNull final List<TaskDTO> taskList = taskService.findAll(USER_ID_ONE);
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2, taskList.size());
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindAllForEmptyUser() {
        taskService.findAll("");
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindAllForNullUser() {
        taskService.findAll((String) null);
    }

    @Test
    public void testFindAllForIncorrectUser() {
        @NotNull final List<TaskDTO> taskList = taskService.findAll("some_id");
        Assert.assertEquals(taskList, Collections.emptyList());
    }

    @Test
    public void testFindAllWithNameComparatorForUser() {
        @NotNull final Sort sortType = Sort.BY_NAME;
        @NotNull final List<TaskDTO> taskSortList = taskService.findAll(USER_ID_TWO, sortType);
        @NotNull final List<TaskDTO> actualTasks = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES / 2; i++) {
            @NotNull final TaskDTO task = new TaskDTO();
            task.setName("Test " + (i + 5));
            actualTasks.add(task);
        }
        actualTasks.add(0, actualTasks.get(NUMBER_OF_ENTRIES / 2 - 1));
        actualTasks.remove(NUMBER_OF_ENTRIES / 2);
        Assert.assertEquals(actualTasks, taskSortList);
    }

    @Test
    public void testFindAllWithCreatedComparatorForUser() {
        @NotNull final Sort sortType = Sort.BY_CREATED;
        @NotNull final List<TaskDTO> taskSortList = taskService.findAll(USER_ID_ONE, sortType);
        @NotNull final List<TaskDTO> actualTasks = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES / 2; i++) {
            @NotNull final TaskDTO task = new TaskDTO();
            task.setName("Test " + i);
            actualTasks.add(task);
        }
        Assert.assertEquals(actualTasks, taskSortList);
    }

    @Test
    public void testFindAllWithStatusComparatorForUser() {
        @NotNull final Sort sortType = Sort.BY_STATUS;
        @NotNull final List<Status> statusList = taskService.findAll(USER_ID_TWO, sortType)
                .stream().map(TaskDTO::getStatus).collect(Collectors.toList());
        @NotNull final List<Status> actualStatuses = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES / 2; i++) {
            if (i < 2) actualStatuses.add(Status.IN_PROGRESS);
            else actualStatuses.add(Status.NOT_STARTED);
        }
        Assert.assertEquals(actualStatuses, statusList);
    }

    @Test
    public void testFindAllWithEmptyComparatorForUser() {
        @NotNull final List<TaskDTO> taskList = taskService.findAll(USER_ID_TWO);
        @NotNull final List<TaskDTO> taskSortList = taskService.findAll(USER_ID_TWO, null);
        Assert.assertEquals(taskList, taskSortList);
    }

    @Test(expected = SortIncorrectException.class)
    public void testFindAllWithIncorrectComparatorForUser() {
        taskService.findAll(USER_ID_TWO, Sort.toSort("123"));
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindAllWithComparatorForEmptyUser() {
        @NotNull final Sort sortType = Sort.BY_NAME;
        taskService.findAll("", sortType);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindAllWithComparatorForNullUser() {
        @NotNull final Sort sortType = Sort.BY_NAME;
        taskService.findAll(null, sortType);
    }

    @Test
    public void testFindAllWithComparatorForIncorrectUser() {
        @NotNull final Sort sortType = Sort.BY_NAME;
        @NotNull final List<TaskDTO> taskList = taskService.findAll("some_id", sortType);
        Assert.assertEquals(taskList, Collections.emptyList());
    }

    @Test
    public void testFindById() {
        @NotNull final TaskDTO task = taskService.findAll().get(0);
        @NotNull final String taskId = taskService.findAll().get(0).getId();
        @Nullable final TaskDTO actualTask = taskService.findOneById(taskId);
        Assert.assertNotNull(actualTask);
        Assert.assertEquals(task.getName(), actualTask.getName());
        Assert.assertEquals(task.getDescription(), actualTask.getDescription());
        Assert.assertEquals(task.getUserId(), actualTask.getUserId());
    }

    @Test(expected = IdEmptyException.class)
    public void testFindByEmptyId() {
        taskService.findOneById("");
    }

    @Test(expected = IdEmptyException.class)
    public void testFindByNullId() {
        taskService.findOneById(null);
    }

    @Test
    public void testFindByIdForUser() {
        @NotNull final TaskDTO task = taskService.findAll(USER_ID_ONE).get(0);
        @NotNull final String taskId = taskService.findAll(USER_ID_ONE).get(0).getId();
        @Nullable final TaskDTO actualTask = taskService.findOneById(USER_ID_ONE, taskId);
        Assert.assertNotNull(actualTask);
        Assert.assertEquals(task.getName(), actualTask.getName());
        Assert.assertEquals(task.getDescription(), actualTask.getDescription());
        Assert.assertEquals(task.getUserId(), actualTask.getUserId());
    }

    @Test(expected = IdEmptyException.class)
    public void testFindByEmptyIdForUser() {
        taskService.findOneById(USER_ID_ONE, "");
    }

    @Test(expected = IdEmptyException.class)
    public void testFindByNullIdForUser() {
        taskService.findOneById(USER_ID_ONE, null);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindByIdForEmptyUser() {
        @NotNull final String taskId = taskService.findAll(USER_ID_ONE).get(0).getId();
        taskService.findOneById("", taskId);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindByIdForNullUser() {
        @NotNull final String projectId = taskService.findAll(USER_ID_ONE).get(0).getId();
        taskService.findOneById(null, projectId);
    }

    @Test
    public void testFindByIdForIncorrectUser() {
        @NotNull final String projectId = taskService.findAll(USER_ID_ONE).get(0).getId();
        Assert.assertNull(taskService.findOneById("some_id", projectId));
    }

    @Test
    public void testFindByIdTaskNotFound() {
        @NotNull final String id = UUID.randomUUID().toString();
        Assert.assertNull(taskService.findOneById(id));
    }

    @Test
    public void testFindByIdTaskNotFoundForUser() {
        @NotNull final String id = UUID.randomUUID().toString();
        Assert.assertNull(taskService.findOneById(USER_ID_ONE, id));
    }

    @Test
    public void testFindByIndex() {
        @NotNull final TaskDTO task = taskService.findAll().get(0);
        @Nullable final TaskDTO actualTask = taskService.findOneByIndex(0);
        Assert.assertNotNull(actualTask);
        Assert.assertEquals(task.getName(), actualTask.getName());
        Assert.assertEquals(task.getDescription(), actualTask.getDescription());
        Assert.assertEquals(task.getUserId(), actualTask.getUserId());
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByEmptyIndex() {
        taskService.findOneByIndex(null);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByNegativeIndex() {
        taskService.findOneByIndex(-2);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByIncorrectIndex() {
        taskService.findOneByIndex((int) taskService.getSize() + 1);
    }

    @Test
    public void testFindByIndexForUser() {
        @NotNull final TaskDTO task = taskService.findAll(USER_ID_TWO).get(0);
        @Nullable final TaskDTO actualTask = taskService.findOneByIndex(USER_ID_TWO, 0);
        Assert.assertNotNull(actualTask);
        Assert.assertEquals(task.getName(), actualTask.getName());
        Assert.assertEquals(task.getDescription(), actualTask.getDescription());
        Assert.assertEquals(task.getUserId(), actualTask.getUserId());
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByEmptyIndexForUser() {
        taskService.findOneByIndex(USER_ID_TWO, null);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByNegativeIndexForUser() {
        taskService.findOneByIndex(USER_ID_TWO, -2);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByIncorrectIndexForUser() {
        taskService.findOneByIndex(USER_ID_TWO, 12);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindByIndexForEmptyUser() {
        taskService.findOneByIndex("", 0);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindByIndexForNullUser() {
        taskService.findOneByIndex(null, 0);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByIndexForIncorrectUser() {
        taskService.findOneByIndex("some_id", 0);
    }

    @Test
    public void testGetSize() {
        final long expectedNumberOfEntries = taskService.getSize() + 1;
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName("Test Task");
        task.setUserId(USER_ID_ONE);
        taskService.add(task);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize());
    }

    @Test
    public void testGetSizeForUser() {
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName("Test Task");
        task.setUserId(USER_ID_TWO);
        taskService.add(task);
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2 + 1, taskService.getSize(USER_ID_TWO));
    }

    @Test(expected = UserIdEmptyException.class)
    public void testGetSizeForEmptyUser() {
        taskService.getSize("");
    }

    @Test(expected = UserIdEmptyException.class)
    public void testGetSizeForNullUser() {
        taskService.getSize(null);
    }

    @Test
    public void testGetSizeForIncorrectUser() {
        Assert.assertEquals(0, taskService.getSize("some_id"));
    }

    @Test
    public void testIsNotFoundById() {
        @NotNull final String validId = taskService.findAll().get(0).getId();
        @NotNull final String invalidId = UUID.randomUUID().toString();
        Assert.assertFalse(taskService.existsById(invalidId));
        Assert.assertTrue(taskService.existsById(validId));
    }

    @Test(expected = IdEmptyException.class)
    public void testIsNotFoundByEmptyId() {
        taskService.existsById("");
    }

    @Test(expected = IdEmptyException.class)
    public void testIsNotFoundByNullId() {
        taskService.existsById(null);
    }

    @Test
    public void testIsNotFoundByIdForUser() {
        @NotNull final String validId = taskService.findAll(USER_ID_ONE).get(0).getId();
        @NotNull final String invalidId = UUID.randomUUID().toString();
        Assert.assertFalse(taskService.existsById(USER_ID_ONE, invalidId));
        Assert.assertTrue(taskService.existsById(USER_ID_ONE, validId));
    }

    @Test(expected = IdEmptyException.class)
    public void testIsNotFoundByEmptyIdForUser() {
        taskService.existsById(USER_ID_ONE, "");
    }

    @Test(expected = IdEmptyException.class)
    public void testIsNotFoundByNullIdForUser() {
        taskService.existsById(USER_ID_ONE, null);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testIsNotFoundForEmptyUser() {
        taskService.existsById("", taskService.findAll(USER_ID_ONE).get(0).getId());
    }

    @Test(expected = UserIdEmptyException.class)
    public void testIsNotFoundForNullUser() {
        taskService.existsById(null, taskService.findAll(USER_ID_ONE).get(0).getId());
    }

    @Test
    public void testIsNotFoundForIncorrectUser() {
        Assert.assertFalse(taskService.existsById("some_id", taskService.findAll(USER_ID_ONE).get(0).getId()));
    }

    @Test
    public void testRemove() {
        final long expectedNumberOfEntries = taskService.getSize(USER_ID_ONE) - 1;
        @NotNull final TaskDTO task = taskService.findAll(USER_ID_ONE).get(0);
        taskService.removeOne(task);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize(USER_ID_ONE));
    }

    @Test(expected = ModelEmptyException.class)
    public void testRemoveNull() {
        taskService.removeOne(null);
    }

    @Test
    public void testRemoveById() {
        final long expectedNumberOfEntries = taskService.getSize() - 1;
        @NotNull final String taskId = taskService.findAll().get(0).getId();
        taskService.removeOneById(taskId);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize());
    }

    @Test
    public void testRemoveByIdForUser() {
        final long expectedNumberOfEntries = taskService.getSize(USER_ID_ONE) - 1;
        @NotNull final String taskId = taskService.findAll(USER_ID_ONE).get(0).getId();
        taskService.removeOneById(USER_ID_ONE, taskId);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize(USER_ID_ONE));
    }

    @Test(expected = IdEmptyException.class)
    public void testRemoveByEmptyId() {
        Assert.assertNull(taskService.removeOneById(""));
    }

    @Test(expected = IdEmptyException.class)
    public void testRemoveByEmptyIdForUser() {
        taskService.removeOneById(USER_ID_ONE, "");
    }

    @Test(expected = IdEmptyException.class)
    public void testRemoveByNullIdForUser() {
        taskService.removeOneById(USER_ID_ONE, null);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testRemoveByIdForEmptyUser() {
        @NotNull final String projectId = taskService.findAll().get(0).getId();
        taskService.removeOneById("", projectId);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testRemoveByIdForNullUser() {
        @NotNull final String projectId = taskService.findAll().get(0).getId();
        taskService.removeOneById(null, projectId);
    }

    @Test
    public void testRemoveByIdForIncorrectUser() {
        @NotNull final String projectId = taskService.findAll().get(0).getId();
        taskService.removeOneById("some_id", projectId);
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskService.getSize());
    }

    @Test
    public void testRemoveByIdTaskNotFound() {
        @NotNull final String invalidId = UUID.randomUUID().toString();
        Assert.assertNull(taskService.removeOneById(invalidId));
    }

    @Test
    public void testRemoveByIdTaskNotFoundForUser() {
        @NotNull final String invalidId = UUID.randomUUID().toString();
        Assert.assertNull(taskService.removeOneById(USER_ID_ONE, invalidId));
    }

    @Test
    public void testRemoveByIndex() {
        final long expectedNumberOfEntries = taskService.getSize() - 1;
        taskService.removeOneByIndex(0);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize());
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByEmptyIndex() {
        taskService.removeOneByIndex(null);
    }

    @Test
    public void testRemoveByIndexForUser() {
        final long expectedNumberOfEntries = taskService.getSize(USER_ID_TWO) - 1;
        taskService.removeOneByIndex(USER_ID_TWO, 0);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize(USER_ID_TWO));
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByEmptyIndexForUser() {
        taskService.removeOneByIndex(USER_ID_TWO, null);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testRemoveByIndexForEmptyUser() {
        taskService.removeOneByIndex("", 0);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testRemoveByIndexForNullUser() {
        taskService.removeOneByIndex(null, 0);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByIndexForIncorrectUser() {
        taskService.removeOneByIndex("some_id", 0);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByNegativeIndex() {
        taskService.removeOneByIndex(-2);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByNegativeIndexForUser() {
        taskService.removeOneByIndex(USER_ID_TWO, -2);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByIncorrectIndex() {
        taskService.removeOneByIndex((int) taskService.getSize() + 1);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByIncorrectIndexForUser() {
        taskService.removeOneByIndex(USER_ID_TWO, 5);
    }

    @Test
    public void testFindAllByProjectId() {
        @NotNull final List<TaskDTO> tasksOne = taskService.findAll(USER_ID_ONE);
        @NotNull final List<TaskDTO> tasks = taskService.findAllByProjectId(USER_ID_ONE, projectOne.getId());
        Assert.assertEquals(tasksOne, tasks);
    }

    @Test
    public void testFindAllByEmptyProjectId() {
        @NotNull final List<TaskDTO> tasks = taskService.findAllByProjectId(USER_ID_ONE, "");
        Assert.assertEquals(Collections.emptyList(), tasks);
    }

    @Test
    public void testFindAllByNullProjectId() {
        @NotNull final List<TaskDTO> tasks = taskService.findAllByProjectId(USER_ID_ONE, null);
        Assert.assertEquals(Collections.emptyList(), tasks);
    }

    @Test
    public void testFindAllByIncorrectProjectId() {
        @NotNull final List<TaskDTO> tasks = taskService.findAllByProjectId(USER_ID_ONE, projectTwo.getId());
        Assert.assertEquals(Collections.emptyList(), tasks);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindAllByProjectIdForEmptyUser() {
        taskService.findAllByProjectId("", projectOne.getId());
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindAllByProjectIdForNullUser() {
        taskService.findAllByProjectId(null, projectOne.getId());
    }

    @Test
    public void testFindAllByProjectIdForIncorrectUser() {
        @NotNull final List<TaskDTO> tasks = taskService.findAllByProjectId("some_id", projectOne.getId());
        Assert.assertEquals(0, tasks.size());
    }

    @Test
    public void testBindTaskToProject() {
        @NotNull final TaskDTO task = taskService.findAllByProjectId(USER_ID_ONE, projectOne.getId()).get(0);
        Assert.assertEquals(projectOne.getId(), task.getProjectId());
        @NotNull final TaskDTO actualTask = projectTaskService.bindTaskToProject(USER_ID_ONE, projectTwo.getId(), task.getId());
        Assert.assertEquals(projectTwo.getId(), actualTask.getProjectId());
    }

    @Test(expected = ProjectIdEmptyException.class)
    public void testBindTaskToEmptyProject() {
        @NotNull final TaskDTO task = taskService.findAllByProjectId(USER_ID_ONE, projectOne.getId()).get(0);
        projectTaskService.bindTaskToProject(USER_ID_ONE, "", task.getId());
    }

    @Test(expected = ProjectIdEmptyException.class)
    public void testBindTaskToNullProject() {
        @NotNull final TaskDTO task = taskService.findAllByProjectId(USER_ID_ONE, projectOne.getId()).get(0);
        projectTaskService.bindTaskToProject(USER_ID_ONE, null, task.getId());
    }

    @Test(expected = TaskIdEmptyException.class)
    public void testBindEmptyTaskToProject() {
        projectTaskService.bindTaskToProject(USER_ID_ONE, projectTwo.getId(), "");
    }

    @Test(expected = TaskIdEmptyException.class)
    public void testBindNullTaskToProject() {
        projectTaskService.bindTaskToProject(USER_ID_ONE, projectTwo.getId(), null);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testBindTaskToIncorrectProject() {
        @NotNull final TaskDTO task = taskService.findAllByProjectId(USER_ID_ONE, projectOne.getId()).get(0);
        projectTaskService.bindTaskToProject(USER_ID_ONE, "some_id", task.getId());
    }

    @Test(expected = TaskNotFoundException.class)
    public void testBindIncorrectTaskToProject() {
        projectTaskService.bindTaskToProject(USER_ID_ONE, projectTwo.getId(), "some_id");
    }

    @Test(expected = UserIdEmptyException.class)
    public void testBindTaskToProjectForEmptyUser() {
        @NotNull final TaskDTO task = taskService.findAllByProjectId(USER_ID_ONE, projectOne.getId()).get(0);
        projectTaskService.bindTaskToProject("", projectTwo.getId(), task.getId());
    }

    @Test(expected = UserIdEmptyException.class)
    public void testBindTaskToProjectForNullUser() {
        @NotNull final TaskDTO task = taskService.findAllByProjectId(USER_ID_ONE, projectOne.getId()).get(0);
        projectTaskService.bindTaskToProject(null, projectTwo.getId(), task.getId());
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testBindTaskToProjectForIncorrectUser() {
        @NotNull final TaskDTO task = taskService.findAllByProjectId(USER_ID_ONE, projectOne.getId()).get(0);
        projectTaskService.bindTaskToProject("some_id", projectTwo.getId(), task.getId());
    }

    @Test
    public void testUnbindTaskFromProject() {
        @NotNull final TaskDTO task = taskService.findAllByProjectId(USER_ID_ONE, projectOne.getId()).get(0);
        Assert.assertEquals(projectOne.getId(), task.getProjectId());
        @NotNull final TaskDTO actualTask = projectTaskService.unbindTaskFromProject(USER_ID_ONE, projectOne.getId(), task.getId());
        Assert.assertNull(actualTask.getProjectId());
    }

    @Test(expected = ProjectIdEmptyException.class)
    public void testUnbindTaskFromEmptyProject() {
        @NotNull final TaskDTO task = taskService.findAllByProjectId(USER_ID_ONE, projectOne.getId()).get(0);
        projectTaskService.unbindTaskFromProject(USER_ID_ONE, "", task.getId());
    }

    @Test(expected = ProjectIdEmptyException.class)
    public void testUnbindTaskFromNullProject() {
        @NotNull final TaskDTO task = taskService.findAllByProjectId(USER_ID_ONE, projectOne.getId()).get(0);
        projectTaskService.unbindTaskFromProject(USER_ID_ONE, null, task.getId());
    }

    @Test(expected = TaskIdEmptyException.class)
    public void testUnbindEmptyTaskFromProject() {
        projectTaskService.unbindTaskFromProject(USER_ID_ONE, projectTwo.getId(), "");
    }

    @Test(expected = TaskIdEmptyException.class)
    public void testUnbindNullTaskFromProject() {
        projectTaskService.unbindTaskFromProject(USER_ID_ONE, projectTwo.getId(), null);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testUnbindTaskFromIncorrectProject() {
        @NotNull final TaskDTO task = taskService.findAllByProjectId(USER_ID_ONE, projectOne.getId()).get(0);
        projectTaskService.unbindTaskFromProject(USER_ID_ONE, "some_id", task.getId());
    }

    @Test(expected = TaskNotFoundException.class)
    public void testUnbindIncorrectTaskFromProject() {
        projectTaskService.unbindTaskFromProject(USER_ID_ONE, projectTwo.getId(), "some_id");
    }

    @Test(expected = UserIdEmptyException.class)
    public void testUnbindTaskFromProjectForEmptyUser() {
        @NotNull final TaskDTO task = taskService.findAllByProjectId(USER_ID_ONE, projectOne.getId()).get(0);
        projectTaskService.unbindTaskFromProject("", projectTwo.getId(), task.getId());
    }

    @Test(expected = UserIdEmptyException.class)
    public void testUnbindTaskFromProjectForNullUser() {
        @NotNull final TaskDTO task = taskService.findAllByProjectId(USER_ID_ONE, projectOne.getId()).get(0);
        projectTaskService.unbindTaskFromProject(null, projectTwo.getId(), task.getId());
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testUnbindTaskFromProjectForIncorrectUser() {
        @NotNull final TaskDTO task = taskService.findAllByProjectId(USER_ID_ONE, projectOne.getId()).get(0);
        projectTaskService.unbindTaskFromProject("some_id", projectTwo.getId(), task.getId());
    }

    @Test
    public void testBindTaskToProjectContinue() {
        @NotNull final TaskDTO task = taskService.findAllByProjectId(USER_ID_ONE, projectOne.getId()).get(0);
        Assert.assertEquals(projectOne.getId(), task.getProjectId());
        @NotNull final TaskDTO actualTask = taskService.bindTaskToProject(USER_ID_ONE, projectTwo.getId(), task.getId());
        Assert.assertEquals(projectTwo.getId(), actualTask.getProjectId());
    }

    @Test(expected = ProjectIdEmptyException.class)
    public void testBindTaskToEmptyProjectContinue() {
        @NotNull final TaskDTO task = taskService.findAllByProjectId(USER_ID_ONE, projectOne.getId()).get(0);
        taskService.bindTaskToProject(USER_ID_ONE, "", task.getId());
    }

    @Test(expected = ProjectIdEmptyException.class)
    public void testBindTaskToNullProjectContinue() {
        @NotNull final TaskDTO task = taskService.findAllByProjectId(USER_ID_ONE, projectOne.getId()).get(0);
        taskService.bindTaskToProject(USER_ID_ONE, null, task.getId());
    }

    @Test(expected = TaskIdEmptyException.class)
    public void testBindEmptyTaskToProjectContinue() {
        taskService.bindTaskToProject(USER_ID_ONE, projectTwo.getId(), "");
    }

    @Test(expected = TaskIdEmptyException.class)
    public void testBindNullTaskToProjectContinue() {
        taskService.bindTaskToProject(USER_ID_ONE, projectTwo.getId(), null);
    }

    @Test(expected = TaskNotFoundException.class)
    public void testBindIncorrectTaskToProjectContinue() {
        taskService.bindTaskToProject(USER_ID_ONE, projectTwo.getId(), "some_id");
    }

    @Test(expected = UserIdEmptyException.class)
    public void testBindTaskToProjectForEmptyUserContinue() {
        @NotNull final TaskDTO task = taskService.findAllByProjectId(USER_ID_ONE, projectOne.getId()).get(0);
        taskService.bindTaskToProject("", projectTwo.getId(), task.getId());
    }

    @Test(expected = UserIdEmptyException.class)
    public void testBindTaskToProjectForNullUserContinue() {
        @NotNull final TaskDTO task = taskService.findAllByProjectId(USER_ID_ONE, projectOne.getId()).get(0);
        taskService.bindTaskToProject(null, projectTwo.getId(), task.getId());
    }

    @Test(expected = TaskNotFoundException.class)
    public void testBindTaskToProjectForIncorrectUserContinue() {
        @NotNull final TaskDTO task = taskService.findAllByProjectId(USER_ID_ONE, projectOne.getId()).get(0);
        taskService.bindTaskToProject("some_id", projectTwo.getId(), task.getId());
    }

    @Test
    public void testUnbindTaskFromProjectContinue() {
        @NotNull final TaskDTO task = taskService.findAllByProjectId(USER_ID_ONE, projectOne.getId()).get(0);
        Assert.assertEquals(projectOne.getId(), task.getProjectId());
        @NotNull final TaskDTO actualTask = taskService.unbindTaskFromProject(USER_ID_ONE, task.getId());
        Assert.assertNull(actualTask.getProjectId());
    }

    @Test(expected = TaskIdEmptyException.class)
    public void testUnbindEmptyTaskFromProjectContinue() {
        taskService.unbindTaskFromProject(USER_ID_ONE, "");
    }

    @Test(expected = TaskIdEmptyException.class)
    public void testUnbindNullTaskFromProjectContinue() {
        taskService.unbindTaskFromProject(USER_ID_ONE, null);
    }

    @Test(expected = TaskNotFoundException.class)
    public void testUnbindIncorrectTaskFromProjectContinue() {
        taskService.unbindTaskFromProject(USER_ID_ONE, "some_id");
    }

    @Test(expected = UserIdEmptyException.class)
    public void testUnbindTaskFromProjectForEmptyUserContinue() {
        @NotNull final TaskDTO task = taskService.findAllByProjectId(USER_ID_ONE, projectOne.getId()).get(0);
        taskService.unbindTaskFromProject("", task.getId());
    }

    @Test(expected = UserIdEmptyException.class)
    public void testUnbindTaskFromProjectForNullUserContinue() {
        @NotNull final TaskDTO task = taskService.findAllByProjectId(USER_ID_ONE, projectOne.getId()).get(0);
        taskService.unbindTaskFromProject(null, task.getId());
    }

    @Test(expected = TaskNotFoundException.class)
    public void testUnbindTaskFromProjectForIncorrectUserContinue() {
        @NotNull final TaskDTO task = taskService.findAllByProjectId(USER_ID_ONE, projectOne.getId()).get(0);
        taskService.unbindTaskFromProject("some_id", task.getId());
    }

    @After
    public void clearRepository() {
        userService.removeAll();
    }

}
