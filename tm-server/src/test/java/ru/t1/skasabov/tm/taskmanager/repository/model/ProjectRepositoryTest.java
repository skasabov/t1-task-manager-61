package ru.t1.skasabov.tm.taskmanager.repository.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.t1.skasabov.tm.enumerated.Status;
import ru.t1.skasabov.tm.model.Project;
import ru.t1.skasabov.tm.model.Task;
import ru.t1.skasabov.tm.model.User;
import ru.t1.skasabov.tm.repository.model.ProjectRepository;
import ru.t1.skasabov.tm.repository.model.TaskRepository;
import ru.t1.skasabov.tm.repository.model.UserRepository;
import ru.t1.skasabov.tm.taskmanager.AbstractTest;

import java.util.*;
import java.util.stream.Collectors;

public class ProjectRepositoryTest extends AbstractTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static User userOther;

    @NotNull
    private static String USER_ID_ONE;

    @NotNull
    private static String USER_ID_TWO;

    @NotNull
    private UserRepository userRepository;

    @NotNull
    private ProjectRepository projectRepository;

    @NotNull
    private TaskRepository taskRepository;

    @Before
    @SneakyThrows
    public void initRepository() {
        userRepository = context.getBean(UserRepository.class);
        @NotNull final User user = new User();
        user.setLogin("user_one");
        user.setPasswordHash("user_one");
        userOther = new User();
        userOther.setLogin("user_two");
        userOther.setPasswordHash("user_two");
        userRepository.save(user);
        userRepository.save(userOther);
        USER_ID_ONE = user.getId();
        USER_ID_TWO = userOther.getId();
        final long currentTime = System.currentTimeMillis();
        taskRepository = context.getBean(TaskRepository.class);
        projectRepository = context.getBean(ProjectRepository.class);
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Project project = new Project();
            project.setName("Test " + i);
            project.setCreated(new Date(currentTime + i * 1000));
            if (i < 4) project.setStatus(Status.COMPLETED);
            else if (i < 7) project.setStatus(Status.IN_PROGRESS);
            else project.setStatus(Status.NOT_STARTED);
            @NotNull final Task task = new Task();
            task.setName("Test " + i);
            task.setProject(project);
            if (i <= 5) {
                project.setUser(user);
                task.setUser(user);
            }
            else {
                project.setUser(userOther);
                task.setUser(userOther);
            }
            @NotNull final List<Task> taskList = new ArrayList<>();
            taskList.add(task);
            project.setTasks(taskList);
            projectRepository.save(project);
        }
    }

    @Test
    public void testDeleteByUserId() {
        final long expectedNumberOfTasks = taskRepository.count() - NUMBER_OF_ENTRIES / 2;
        final long expectedNumberOfEntries = projectRepository.count() - NUMBER_OF_ENTRIES / 2;
        projectRepository.deleteByUserId(USER_ID_ONE);
        Assert.assertEquals(expectedNumberOfTasks, taskRepository.count());
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.count());
    }

    @Test
    public void testDeleteByEmptyUserId() {
        projectRepository.deleteByUserId("");
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectRepository.count());
    }

    @Test
    public void testDeleteByIncorrectUserId() {
        projectRepository.deleteByUserId("some_id");
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectRepository.count());
    }

    @Test
    public void testFindByUserId() {
        @NotNull final List<Project> projectList = projectRepository.findByUserId(USER_ID_ONE);
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2, projectList.size());
    }

    @Test
    public void testFindByEmptyUserId() {
        @NotNull final List<Project> projectList = projectRepository.findByUserId("");
        Assert.assertEquals(0, projectList.size());
    }

    @Test
    public void testFindByIncorrectUserId() {
        @NotNull final List<Project> projectList = projectRepository.findByUserId("some_id");
        Assert.assertEquals(0, projectList.size());
    }

    @Test
    public void testFindAllSortByNameForUser() {
        @NotNull final List<Project> projectSortList = projectRepository.findAllSortByNameForUser(USER_ID_TWO);
        @NotNull final List<Project> actualProjects = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES / 2; i++) {
            @NotNull final Project project = new Project();
            project.setName("Test " + (i + 5));
            actualProjects.add(project);
        }
        actualProjects.add(0, actualProjects.get(NUMBER_OF_ENTRIES / 2 - 1));
        actualProjects.remove(NUMBER_OF_ENTRIES / 2);
        Assert.assertEquals(actualProjects, projectSortList);
    }

    @Test
    public void testFindAllSortByNameForEmptyUser() {
        @NotNull final List<Project> projectSortList = projectRepository.findAllSortByNameForUser("");
        Assert.assertEquals(Collections.emptyList(), projectSortList);
    }

    @Test
    public void testFindAllSortByNameForIncorrectUser() {
        @NotNull final List<Project> projectSortList = projectRepository.findAllSortByNameForUser("some_id");
        Assert.assertEquals(Collections.emptyList(), projectSortList);
    }

    @Test
    public void testFindAllSortByCreatedForUser() {
        @NotNull final List<Project> projectSortList = projectRepository.findAllSortByCreatedForUser(USER_ID_ONE);
        @NotNull final List<Project> actualProjects = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES / 2; i++) {
            @NotNull final Project project = new Project();
            project.setName("Test " + i);
            actualProjects.add(project);
        }
        Assert.assertEquals(actualProjects, projectSortList);
    }

    @Test
    public void testFindAllSortByCreatedForEmptyUser() {
        @NotNull final List<Project> projectSortList = projectRepository.findAllSortByCreatedForUser("");
        Assert.assertEquals(Collections.emptyList(), projectSortList);
    }

    @Test
    public void testFindAllSortByCreatedForIncorrectUser() {
        @NotNull final List<Project> projectSortList = projectRepository.findAllSortByCreatedForUser("some_id");
        Assert.assertEquals(Collections.emptyList(), projectSortList);
    }

    @Test
    public void testFindAllSortByStatusForUser() {
        @NotNull final List<Status> statusList = projectRepository.findAllSortByStatusForUser(USER_ID_TWO)
                .stream().map(Project::getStatus).collect(Collectors.toList());
        @NotNull final List<Status> actualStatuses = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES / 2; i++) {
            if (i < 2) actualStatuses.add(Status.IN_PROGRESS);
            else actualStatuses.add(Status.NOT_STARTED);
        }
        Assert.assertEquals(actualStatuses, statusList);
    }

    @Test
    public void testFindAllSortByStatusForEmptyUser() {
        @NotNull final List<Project> projectSortList = projectRepository.findAllSortByStatusForUser("");
        Assert.assertEquals(Collections.emptyList(), projectSortList);
    }

    @Test
    public void testFindAllSortByStatusForIncorrectUser() {
        @NotNull final List<Project> projectSortList = projectRepository.findAllSortByStatusForUser("some_id");
        Assert.assertEquals(Collections.emptyList(), projectSortList);
    }

    @Test
    public void testFindByUserIdAndId() {
        @NotNull final Project project = projectRepository.findByUserId(USER_ID_ONE).get(0);
        @NotNull final String projectId = projectRepository.findByUserId(USER_ID_ONE).get(0).getId();
        @Nullable final Project actualProject = projectRepository.findByUserIdAndId(USER_ID_ONE, projectId);
        Assert.assertNotNull(actualProject);
        Assert.assertEquals(project.getName(), actualProject.getName());
        Assert.assertEquals(project.getDescription(), actualProject.getDescription());
        Assert.assertEquals(project.getUser().getId(), actualProject.getUser().getId());
    }

    @Test
    public void testFindByUserIdAndEmptyId() {
        Assert.assertNull(projectRepository.findByUserIdAndId(USER_ID_ONE, ""));
    }

    @Test
    public void testFindByUserIdAndIncorrectId() {
        @NotNull final String id = UUID.randomUUID().toString();
        Assert.assertNull(projectRepository.findByUserIdAndId(USER_ID_ONE, id));
    }

    @Test
    public void testFindByEmptyUserIdAndId() {
        @NotNull final String projectId = projectRepository.findByUserId(USER_ID_ONE).get(0).getId();
        Assert.assertNull(projectRepository.findByUserIdAndId("", projectId));
    }

    @Test
    public void testFindByIncorrectUserIdAndId() {
        @NotNull final String projectId = projectRepository.findByUserId(USER_ID_ONE).get(0).getId();
        Assert.assertNull(projectRepository.findByUserIdAndId("some_id", projectId));
    }

    @Test
    public void testFindByUserIdAndIndex() {
        @NotNull final Project project = projectRepository.findByUserId(USER_ID_TWO).get(0);
        @Nullable final Project actualProject = projectRepository.findOneByIndex(USER_ID_TWO, 0);
        Assert.assertNotNull(actualProject);
        Assert.assertEquals(project.getName(), actualProject.getName());
        Assert.assertEquals(project.getDescription(), actualProject.getDescription());
        Assert.assertEquals(project.getUser().getId(), actualProject.getUser().getId());
        Assert.assertNull(projectRepository.findOneByIndex(USER_ID_TWO, NUMBER_OF_ENTRIES / 2));
        Assert.assertNull(projectRepository.findOneByIndex("", NUMBER_OF_ENTRIES / 2));
        Assert.assertNull(projectRepository.findOneByIndex("some_id", NUMBER_OF_ENTRIES / 2));
    }

    @Test
    public void testCountByUserId() {
        @NotNull final Project project = new Project();
        project.setName("Test Project");
        project.setUser(userOther);
        projectRepository.save(project);
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2 + 1, projectRepository.countByUserId(USER_ID_TWO));
        Assert.assertEquals(0, projectRepository.countByUserId(""));
        Assert.assertEquals(0, projectRepository.countByUserId("some_id"));
    }

    @Test
    public void testExistsByUserIdAndId() {
        @NotNull final String validId = projectRepository.findByUserId(USER_ID_ONE).get(0).getId();
        @NotNull final String invalidId = UUID.randomUUID().toString();
        Assert.assertFalse(projectRepository.existsByUserIdAndId("", validId));
        Assert.assertFalse(projectRepository.existsByUserIdAndId("some_id", validId));
        Assert.assertFalse(projectRepository.existsByUserIdAndId(USER_ID_ONE, ""));
        Assert.assertFalse(projectRepository.existsByUserIdAndId(USER_ID_ONE, invalidId));
        Assert.assertTrue(projectRepository.existsByUserIdAndId(USER_ID_ONE, validId));
    }

    @Test
    public void testDeleteByUserIdAndId() {
        final long expectedNumberOfTasks = taskRepository.countByUserId(USER_ID_ONE) - 1;
        final long expectedNumberOfEntries = projectRepository.countByUserId(USER_ID_ONE) - 1;
        @NotNull final String projectId = projectRepository.findByUserId(USER_ID_ONE).get(0).getId();
        projectRepository.deleteByUserIdAndId(USER_ID_ONE, projectId);
        Assert.assertEquals(expectedNumberOfTasks, taskRepository.countByUserId(USER_ID_ONE));
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.countByUserId(USER_ID_ONE));
    }

    @Test
    public void testDeleteByUserIdAndEmptyId() {
        projectRepository.deleteByUserIdAndId(USER_ID_ONE, "");
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectRepository.count());
    }

    @Test
    public void testDeleteByUserIdAndIncorrectId() {
        projectRepository.deleteByUserIdAndId(USER_ID_ONE, "some_id");
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectRepository.count());
    }

    @Test
    public void testDeleteByEmptyUserIdAndId() {
        @NotNull final String projectId = projectRepository.findByUserId(USER_ID_ONE).get(0).getId();
        projectRepository.deleteByUserIdAndId("", projectId);
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectRepository.count());
    }

    @Test
    public void testDeleteByIncorrectUserIdAndId() {
        @NotNull final String projectId = projectRepository.findByUserId(USER_ID_ONE).get(0).getId();
        projectRepository.deleteByUserIdAndId("some_id", projectId);
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectRepository.count());
    }

    @After
    @SneakyThrows
    public void clearRepository() {
        userRepository.deleteAll();
    }

}
