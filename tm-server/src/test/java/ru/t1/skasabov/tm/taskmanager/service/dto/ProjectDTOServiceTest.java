package ru.t1.skasabov.tm.taskmanager.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.t1.skasabov.tm.api.service.dto.IProjectDTOService;
import ru.t1.skasabov.tm.api.service.dto.ITaskDTOService;
import ru.t1.skasabov.tm.api.service.dto.IUserDTOService;
import ru.t1.skasabov.tm.dto.model.ProjectDTO;
import ru.t1.skasabov.tm.dto.model.TaskDTO;
import ru.t1.skasabov.tm.enumerated.Sort;
import ru.t1.skasabov.tm.enumerated.Status;
import ru.t1.skasabov.tm.exception.entity.ModelEmptyException;
import ru.t1.skasabov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.skasabov.tm.exception.field.*;
import ru.t1.skasabov.tm.taskmanager.AbstractTest;

import java.util.*;
import java.util.stream.Collectors;

public class ProjectDTOServiceTest extends AbstractTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static String USER_ID_ONE;

    @NotNull
    private static String USER_ID_TWO;

    @NotNull
    private IProjectDTOService projectService;

    @NotNull
    private ITaskDTOService taskService;

    @NotNull
    private IUserDTOService userService;

    @Before
    public void initTest() {
        final long currentTime = System.currentTimeMillis();
        userService = context.getBean(IUserDTOService.class);
        projectService = context.getBean(IProjectDTOService.class);
        taskService = context.getBean(ITaskDTOService.class);
        USER_ID_ONE = userService.create("user_one", "user_one").getId();
        USER_ID_TWO = userService.create("user_two", "user_two").getId();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final ProjectDTO project = new ProjectDTO();
            project.setName("Test " + i);
            project.setCreated(new Date(currentTime + i * 1000));
            if (i < 4) project.setStatus(Status.COMPLETED);
            else if (i < 7) project.setStatus(Status.IN_PROGRESS);
            else project.setStatus(Status.NOT_STARTED);
            @NotNull final TaskDTO task = new TaskDTO();
            task.setName("Test " + i);
            task.setProjectId(project.getId());
            if (i <= 5) {
                project.setUserId(USER_ID_ONE);
                task.setUserId(USER_ID_ONE);
            }
            else {
                project.setUserId(USER_ID_TWO);
                task.setUserId(USER_ID_TWO);
            }
            projectService.add(project);
            taskService.add(task);
        }
    }

    @Test(expected = UserIdEmptyException.class)
    public void testCreateForEmptyUser() {
        projectService.create("", "test", "");
    }

    @Test(expected = UserIdEmptyException.class)
    public void testCreateForNullUser() {
        projectService.create(null, "test", "");
    }

    @Test(expected = NameEmptyException.class)
    public void testCreateEmptyName() {
        projectService.create(USER_ID_ONE, "", "");
    }

    @Test(expected = NameEmptyException.class)
    public void testCreateNullName() {
        projectService.create(USER_ID_ONE, null, "");
    }

    @Test
    public void testCreateName() {
        final long expectedNumberOfEntries = projectService.getSize() + 1;
        @NotNull final String name = "Test Project";
        @NotNull final ProjectDTO actualProject = projectService.create(USER_ID_ONE, name, "");
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize());
        Assert.assertEquals(USER_ID_ONE, actualProject.getUserId());
        Assert.assertEquals(name, actualProject.getName());
        Assert.assertEquals("", actualProject.getDescription());
    }

    @Test
    public void testCreateDescription() {
        final long expectedNumberOfEntries = projectService.getSize() + 1;
        @NotNull final String name = "Test Project";
        @NotNull final String description = "Test Description";
        @NotNull final ProjectDTO actualProject = projectService.create(USER_ID_TWO, name, description);
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize());
        Assert.assertEquals(USER_ID_TWO, actualProject.getUserId());
        Assert.assertEquals(name, actualProject.getName());
        Assert.assertEquals(description, actualProject.getDescription());
    }

    @Test
    public void createProject() {
        final long expectedNumberOfEntries = projectService.getSize() + 1;
        @NotNull final String name = "Test Project";
        @NotNull final String description = "Test Description";
        @NotNull final Date dateBegin = new Date();
        @NotNull final Date dateEnd = new Date();
        @NotNull final ProjectDTO actualProject = projectService.create(
                USER_ID_ONE, name, description, dateBegin, dateEnd
        );
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize());
        Assert.assertEquals(USER_ID_ONE, actualProject.getUserId());
        Assert.assertEquals(name, actualProject.getName());
        Assert.assertEquals(description, actualProject.getDescription());
        Assert.assertEquals(dateBegin, actualProject.getDateBegin());
        Assert.assertEquals(dateEnd, actualProject.getDateEnd());
    }

    @Test
    public void testUpdateById() {
        @NotNull final String id = projectService.findAll(USER_ID_TWO).get(0).getId();
        @NotNull final String name = "Test Project One";
        @NotNull final String description = "Test Description One";
        @NotNull final ProjectDTO project = projectService.updateById(USER_ID_TWO, id, name, description);
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_ID_TWO, project.getUserId());
        Assert.assertEquals(name, project.getName());
        Assert.assertEquals(description, project.getDescription());
    }

    @Test(expected = IdEmptyException.class)
    public void testUpdateByEmptyId() {
        @NotNull final String name = "Test Project One";
        @NotNull final String description = "Test Description One";
        projectService.updateById(USER_ID_TWO, "", name, description);
    }

    @Test(expected = IdEmptyException.class)
    public void testUpdateByNullId() {
        @NotNull final String name = "Test Project One";
        @NotNull final String description = "Test Description One";
        projectService.updateById(USER_ID_TWO, null, name, description);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testUpdateByIdForEmptyUser() {
        @NotNull final String id = projectService.findAll(USER_ID_TWO).get(0).getId();
        @NotNull final String name = "Test Project One";
        @NotNull final String description = "Test Description One";
        projectService.updateById("", id, name, description);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testUpdateByIdForNullUser() {
        @NotNull final String id = projectService.findAll(USER_ID_TWO).get(0).getId();
        @NotNull final String name = "Test Project One";
        @NotNull final String description = "Test Description One";
        projectService.updateById(null, id, name, description);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testUpdateByIdForIncorrectUser() {
        @NotNull final String id = projectService.findAll(USER_ID_TWO).get(0).getId();
        @NotNull final String name = "Test Project One";
        @NotNull final String description = "Test Description One";
        projectService.updateById("some_id", id, name, description);
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateByIdWithEmptyName() {
        @NotNull final String id = projectService.findAll(USER_ID_TWO).get(0).getId();
        @NotNull final String description = "Test Description One";
        projectService.updateById(USER_ID_TWO, id, "", description);
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateByIdWithNullName() {
        @NotNull final String id = projectService.findAll(USER_ID_TWO).get(0).getId();
        @NotNull final String description = "Test Description One";
        projectService.updateById(USER_ID_TWO, id, null, description);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testUpdateByIdProjectNotFound() {
        @NotNull final String id = UUID.randomUUID().toString();
        @NotNull final String name = "Test Project One";
        @NotNull final String description = "Test Description One";
        projectService.updateById(USER_ID_TWO, id, name, description);
    }

    @Test
    public void testUpdateByIndex() {
        @NotNull final String name = "Test Project One";
        @NotNull final String description = "Test Description One";
        @NotNull final ProjectDTO project = projectService.updateByIndex(USER_ID_ONE, 1, name, description);
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_ID_ONE, project.getUserId());
        Assert.assertEquals(name, project.getName());
        Assert.assertEquals(description, project.getDescription());
    }

    @Test(expected = IndexIncorrectException.class)
    public void testUpdateByEmptyIndex() {
        @NotNull final String name = "Test Project One";
        @NotNull final String description = "Test Description One";
        projectService.updateByIndex(USER_ID_ONE, null, name, description);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testUpdateByIndexForEmptyUser() {
        @NotNull final String name = "Test Project One";
        @NotNull final String description = "Test Description One";
        projectService.updateByIndex("", 1, name, description);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testUpdateByIndexForNullUser() {
        @NotNull final String name = "Test Project One";
        @NotNull final String description = "Test Description One";
        projectService.updateByIndex(null, 1, name, description);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testUpdateByIndexForIncorrectUser() {
        @NotNull final String name = "Test Project One";
        @NotNull final String description = "Test Description One";
        projectService.updateByIndex("some_id", 1, name, description);
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateByIndexWithEmptyName() {
        @NotNull final String description = "Test Description One";
        projectService.updateByIndex(USER_ID_ONE, 1, "", description);
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateByIndexWithNullName() {
        @NotNull final String description = "Test Description One";
        projectService.updateByIndex(USER_ID_ONE, 1, null, description);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testUpdateByNegativeIndex() {
        @NotNull final String name = "Test Project One";
        @NotNull final String description = "Test Description One";
        projectService.updateByIndex(USER_ID_ONE, -2, name, description);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testUpdateByIncorrectIndex() {
        @NotNull final String name = "Test Project One";
        @NotNull final String description = "Test Description One";
        projectService.updateByIndex(USER_ID_ONE, 5, name, description);
    }

    @Test
    public void testChangeProjectStatusById() {
        @NotNull final String id = projectService.findAll(USER_ID_TWO).get(0).getId();
        @NotNull final Status status = Status.NOT_STARTED;
        @NotNull final ProjectDTO project = projectService.changeProjectStatusById(USER_ID_TWO, id, status);
        Assert.assertEquals(USER_ID_TWO, project.getUserId());
        Assert.assertEquals(status, project.getStatus());
    }

    @Test(expected = IdEmptyException.class)
    public void testChangeProjectStatusByEmptyId() {
        @NotNull final Status status = Status.NOT_STARTED;
        projectService.changeProjectStatusById(USER_ID_TWO, "", status);
    }

    @Test(expected = IdEmptyException.class)
    public void testChangeProjectStatusByNullId() {
        @NotNull final Status status = Status.NOT_STARTED;
        projectService.changeProjectStatusById(USER_ID_TWO, null, status);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testChangeProjectStatusByIdForEmptyUser() {
        @NotNull final String id = projectService.findAll(USER_ID_TWO).get(0).getId();
        @NotNull final Status status = Status.NOT_STARTED;
        projectService.changeProjectStatusById("", id, status);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testChangeProjectStatusByIdForNullUser() {
        @NotNull final String id = projectService.findAll(USER_ID_TWO).get(0).getId();
        @NotNull final Status status = Status.NOT_STARTED;
        projectService.changeProjectStatusById(null, id, status);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testChangeProjectStatusByIdForIncorrectUser() {
        @NotNull final String id = projectService.findAll(USER_ID_TWO).get(0).getId();
        @NotNull final Status status = Status.NOT_STARTED;
        projectService.changeProjectStatusById("some_id", id, status);
    }

    @Test(expected = StatusEmptyException.class)
    public void testChangeProjectStatusByIdWithEmptyStatus() {
        @NotNull final String id = projectService.findAll(USER_ID_TWO).get(0).getId();
        projectService.changeProjectStatusById(USER_ID_TWO, id, Status.toStatus(null));
    }

    @Test(expected = StatusIncorrectException.class)
    public void testChangeProjectStatusByIdWithIncorrectStatus() {
        @NotNull final String id = projectService.findAll(USER_ID_TWO).get(0).getId();
        projectService.changeProjectStatusById(USER_ID_TWO, id, Status.toStatus("123"));
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testChangeProjectStatusByIdProjectNotFound() {
        @NotNull final String id = UUID.randomUUID().toString();
        @NotNull final Status status = Status.NOT_STARTED;
        projectService.changeProjectStatusById(USER_ID_TWO, id, status);
    }

    @Test
    public void testChangeProjectStatusByIndex() {
        @NotNull final Status status = Status.NOT_STARTED;
        @NotNull final ProjectDTO project = projectService.changeProjectStatusByIndex(USER_ID_ONE, 1, status);
        Assert.assertEquals(USER_ID_ONE, project.getUserId());
        Assert.assertEquals(status, project.getStatus());
    }

    @Test(expected = IndexIncorrectException.class)
    public void testChangeProjectStatusByEmptyIndex() {
        @NotNull final Status status = Status.NOT_STARTED;
        projectService.changeProjectStatusByIndex(USER_ID_ONE, null, status);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testChangeProjectStatusByIndexForEmptyUser() {
        @NotNull final Status status = Status.NOT_STARTED;
        projectService.changeProjectStatusByIndex("", 1, status);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testChangeProjectStatusByIndexForNullUser() {
        @NotNull final Status status = Status.NOT_STARTED;
        projectService.changeProjectStatusByIndex(null, 1, status);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testChangeProjectStatusByIndexForIncorrectUser() {
        @NotNull final Status status = Status.NOT_STARTED;
        projectService.changeProjectStatusByIndex("some_id", 1, status);
    }

    @Test(expected = StatusEmptyException.class)
    public void testChangeProjectStatusByIndexWithEmptyStatus() {
        projectService.changeProjectStatusByIndex(USER_ID_ONE, 1, Status.toStatus(null));
    }

    @Test(expected = StatusIncorrectException.class)
    public void testChangeProjectStatusByIndexWithIncorrectStatus() {
        projectService.changeProjectStatusByIndex(USER_ID_ONE, 1, Status.toStatus("123"));
    }

    @Test(expected = IndexIncorrectException.class)
    public void testChangeProjectStatusByIncorrectIndex() {
        @NotNull final Status status = Status.NOT_STARTED;
        projectService.changeProjectStatusByIndex(USER_ID_ONE, 5, status);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testChangeProjectStatusByNegativeIndex() {
        @NotNull final Status status = Status.NOT_STARTED;
        projectService.changeProjectStatusByIndex(USER_ID_ONE, -2, status);
    }

    @Test
    public void testAdd() {
        final long expectedNumberOfEntries = projectService.getSize() + 1;
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName("Test Project");
        project.setUserId(USER_ID_TWO);
        projectService.add(project);
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize());
    }

    @Test(expected = ModelEmptyException.class)
    public void testAddNull() {
        projectService.add(null);
    }

    @Test
    public void testAddAll() {
        final long expectedNumberOfEntries = projectService.getSize() + 4;
        @NotNull final List<ProjectDTO> actualProjects = new ArrayList<>();
        for (int i = 1; i <= 4; i++) {
            @NotNull final ProjectDTO project = new ProjectDTO();
            project.setName("Test Project " + i);
            project.setUserId(USER_ID_ONE);
            actualProjects.add(project);
        }
        projectService.addAll(actualProjects);
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize());
    }

    @Test
    public void testAddAllNull() {
        final long expectedNumberOfEntries = projectService.getSize();
        projectService.addAll(null);
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize());
    }

    @Test
    public void testAddAllEmpty() {
        final long expectedNumberOfEntries = projectService.getSize();
        projectService.addAll(Collections.emptyList());
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize());
    }

    @Test
    public void testSet() {
        @NotNull final List<ProjectDTO> actualProjects = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final ProjectDTO project = new ProjectDTO();
            project.setName("Test Project " + i);
            project.setUserId(USER_ID_TWO);
            actualProjects.add(project);
        }
        projectService.set(actualProjects);
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectService.getSize());
        Assert.assertEquals(0, taskService.getSize());
    }

    @Test
    public void testSetNull() {
        final long expectedNumberOfEntries = projectService.getSize();
        projectService.set(null);
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize());
    }

    @Test
    public void testSetEmpty() {
        projectService.set(Collections.emptyList());
        Assert.assertEquals(0, projectService.getSize());
    }

    @Test
    public void testClearAll() {
        projectService.removeAll();
        Assert.assertEquals(0, projectService.getSize());
        Assert.assertEquals(0, taskService.getSize());
    }

    @Test
    public void testClearAllForUser() {
        final long expectedNumberOfEntries = projectService.getSize() - NUMBER_OF_ENTRIES / 2;
        final long expectedNumberOfTasks = taskService.getSize() - NUMBER_OF_ENTRIES / 2;
        projectService.removeAll(USER_ID_ONE);
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize());
        Assert.assertEquals(expectedNumberOfTasks, taskService.getSize());
    }

    @Test(expected = UserIdEmptyException.class)
    public void testClearAllForEmptyUser() {
        projectService.removeAll("");
    }

    @Test(expected = UserIdEmptyException.class)
    public void testClearAllForNullUser() {
        projectService.removeAll((String) null);
    }

    @Test
    public void testClearAllForIncorrectUser() {
        projectService.removeAll("some_id");
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectService.getSize());
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskService.getSize());
    }

    @Test
    public void testClear() {
        final long expectedNumberOfEntries = projectService.getSize() - NUMBER_OF_ENTRIES / 2;
        final long expectedNumberOfTasks = taskService.getSize() - NUMBER_OF_ENTRIES / 2;
        @NotNull final List<ProjectDTO> projectList = projectService.findAll(USER_ID_TWO);
        projectService.removeAll(projectList);
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize());
        Assert.assertEquals(expectedNumberOfTasks, taskService.getSize());
    }

    @Test
    public void testClearNull() {
        projectService.removeAll((List<ProjectDTO>) null);
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectService.getSize());
    }

    @Test
    public void testClearEmpty() {
        projectService.removeAll(Collections.emptyList());
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectService.getSize());
    }

    @Test
    public void testFindAll() {
        @NotNull final List<ProjectDTO> projectList = projectService.findAll();
        Assert.assertEquals(projectList.size(), projectService.getSize());
    }

    @Test
    public void testFindAllWithNameComparator() {
        @NotNull final Sort sortType = Sort.BY_NAME;
        @NotNull final List<ProjectDTO> projectSortList = projectService.findAll(sortType);
        @NotNull final List<ProjectDTO> actualProjects = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final ProjectDTO project = new ProjectDTO();
            project.setName("Test " + i);
            actualProjects.add(project);
        }
        actualProjects.add(1, actualProjects.get(NUMBER_OF_ENTRIES - 1));
        actualProjects.remove(NUMBER_OF_ENTRIES);
        Assert.assertEquals(actualProjects, projectSortList);
    }

    @Test
    public void testFindAllWithCreatedComparator() {
        @NotNull final Sort sortType = Sort.BY_CREATED;
        @NotNull final List<ProjectDTO> projectSortList = projectService.findAll(sortType);
        @NotNull final List<ProjectDTO> actualProjects = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final ProjectDTO project = new ProjectDTO();
            project.setName("Test " + i);
            actualProjects.add(project);
        }
        Assert.assertEquals(actualProjects, projectSortList);
    }

    @Test
    public void testFindAllWithStatusComparator() {
        @NotNull final Sort sortType = Sort.BY_STATUS;
        @NotNull final List<Status> statusList = projectService.findAll(sortType)
                .stream().map(ProjectDTO::getStatus).collect(Collectors.toList());
        @NotNull final List<Status> actualStatuses = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            if (i < 4) actualStatuses.add(Status.COMPLETED);
            else if (i < 7) actualStatuses.add(Status.IN_PROGRESS);
            else actualStatuses.add(Status.NOT_STARTED);
        }
        Assert.assertEquals(actualStatuses, statusList);
    }

    @Test
    public void testFindAllWithEmptyComparator() {
        @NotNull final List<ProjectDTO> projectList = projectService.findAll();
        @NotNull final List<ProjectDTO> projectSortList = projectService.findAll((Sort) null);
        Assert.assertEquals(projectList, projectSortList);
    }

    @Test(expected = SortIncorrectException.class)
    public void testFindAllWithIncorrectComparator() {
        projectService.findAll(Sort.toSort("123"));
    }

    @Test
    public void testFindAllForUser() {
        @NotNull final List<ProjectDTO> projectList = projectService.findAll(USER_ID_ONE);
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2, projectList.size());
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindAllForEmptyUser() {
        projectService.findAll("");
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindAllForNullUser() {
        projectService.findAll((String) null);
    }

    @Test
    public void testFindAllForIncorrectUser() {
        @NotNull final List<ProjectDTO> projectList = projectService.findAll("some_id");
        Assert.assertEquals(projectList, Collections.emptyList());
    }

    @Test
    public void testFindAllWithNameComparatorForUser() {
        @NotNull final Sort sortType = Sort.BY_NAME;
        @NotNull final List<ProjectDTO> projectSortList = projectService.findAll(USER_ID_TWO, sortType);
        @NotNull final List<ProjectDTO> actualProjects = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES / 2; i++) {
            @NotNull final ProjectDTO project = new ProjectDTO();
            project.setName("Test " + (i + 5));
            actualProjects.add(project);
        }
        actualProjects.add(0, actualProjects.get(NUMBER_OF_ENTRIES / 2 - 1));
        actualProjects.remove(NUMBER_OF_ENTRIES / 2);
        Assert.assertEquals(actualProjects, projectSortList);
    }

    @Test
    public void testFindAllWithCreatedComparatorForUser() {
        @NotNull final Sort sortType = Sort.BY_CREATED;
        @NotNull final List<ProjectDTO> projectSortList = projectService.findAll(USER_ID_ONE, sortType);
        @NotNull final List<ProjectDTO> actualProjects = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES / 2; i++) {
            @NotNull final ProjectDTO project = new ProjectDTO();
            project.setName("Test " + i);
            actualProjects.add(project);
        }
        Assert.assertEquals(actualProjects, projectSortList);
    }

    @Test
    public void testFindAllWithStatusComparatorForUser() {
        @NotNull final Sort sortType = Sort.BY_STATUS;
        @NotNull final List<Status> statusList = projectService.findAll(USER_ID_TWO, sortType)
                .stream().map(ProjectDTO::getStatus).collect(Collectors.toList());
        @NotNull final List<Status> actualStatuses = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES / 2; i++) {
            if (i < 2) actualStatuses.add(Status.IN_PROGRESS);
            else actualStatuses.add(Status.NOT_STARTED);
        }
        Assert.assertEquals(actualStatuses, statusList);
    }

    @Test
    public void testFindAllWithEmptyComparatorForUser() {
        @NotNull final List<ProjectDTO> projectList = projectService.findAll(USER_ID_TWO);
        @NotNull final List<ProjectDTO> projectSortList = projectService.findAll(USER_ID_TWO, null);
        Assert.assertEquals(projectList, projectSortList);
    }

    @Test(expected = SortIncorrectException.class)
    public void testFindAllWithIncorrectComparatorForUser() {
        projectService.findAll(USER_ID_TWO, Sort.toSort("123"));
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindAllWithComparatorForEmptyUser() {
        @NotNull final Sort sortType = Sort.BY_NAME;
        projectService.findAll("", sortType);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindAllWithComparatorForNullUser() {
        @NotNull final Sort sortType = Sort.BY_NAME;
        projectService.findAll(null, sortType);
    }

    @Test
    public void testFindAllWithComparatorForIncorrectUser() {
        @NotNull final Sort sortType = Sort.BY_NAME;
        @NotNull final List<ProjectDTO> projectList = projectService.findAll("some_id", sortType);
        Assert.assertEquals(projectList, Collections.emptyList());
    }

    @Test
    public void testFindById() {
        @Nullable final ProjectDTO project = projectService.findAll().get(0);
        Assert.assertNotNull(project);
        @NotNull final String projectId = projectService.findAll().get(0).getId();
        @Nullable final ProjectDTO actualProject = projectService.findOneById(projectId);
        Assert.assertNotNull(actualProject);
        Assert.assertEquals(project.getName(), actualProject.getName());
        Assert.assertEquals(project.getDescription(), actualProject.getDescription());
        Assert.assertEquals(project.getUserId(), actualProject.getUserId());
    }

    @Test(expected = IdEmptyException.class)
    public void testFindByEmptyId() {
        projectService.findOneById("");
    }

    @Test(expected = IdEmptyException.class)
    public void testFindByNullId() {
        projectService.findOneById(null);
    }

    @Test
    public void testFindByIdForUser() {
        @Nullable final ProjectDTO project = projectService.findAll(USER_ID_ONE).get(0);
        Assert.assertNotNull(project);
        @NotNull final String projectId = projectService.findAll(USER_ID_ONE).get(0).getId();
        @Nullable final ProjectDTO actualProject = projectService.findOneById(USER_ID_ONE, projectId);
        Assert.assertNotNull(actualProject);
        Assert.assertEquals(project.getName(), actualProject.getName());
        Assert.assertEquals(project.getDescription(), actualProject.getDescription());
        Assert.assertEquals(project.getUserId(), actualProject.getUserId());
    }

    @Test(expected = IdEmptyException.class)
    public void testFindByEmptyIdForUser() {
        projectService.findOneById(USER_ID_ONE, "");
    }

    @Test(expected = IdEmptyException.class)
    public void testFindByNullIdForUser() {
        projectService.findOneById(USER_ID_ONE, null);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindByIdForEmptyUser() {
        @NotNull final String projectId = projectService.findAll(USER_ID_ONE).get(0).getId();
        projectService.findOneById("", projectId);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindByIdForNullUser() {
        @NotNull final String projectId = projectService.findAll(USER_ID_ONE).get(0).getId();
        projectService.findOneById(null, projectId);
    }

    @Test
    public void testFindByIdForIncorrectUser() {
        @NotNull final String projectId = projectService.findAll(USER_ID_ONE).get(0).getId();
        Assert.assertNull(projectService.findOneById("some_id", projectId));
    }

    @Test
    public void testFindByIdProjectNotFound() {
        @NotNull final String id = UUID.randomUUID().toString();
        Assert.assertNull(projectService.findOneById(id));
    }

    @Test
    public void testFindByIdProjectNotFoundForUser() {
        @NotNull final String id = UUID.randomUUID().toString();
        Assert.assertNull(projectService.findOneById(USER_ID_ONE, id));
    }

    @Test
    public void testFindByIndex() {
        @NotNull final ProjectDTO project = projectService.findAll().get(0);
        @Nullable final ProjectDTO actualProject = projectService.findOneByIndex(0);
        Assert.assertNotNull(actualProject);
        Assert.assertEquals(project.getName(), actualProject.getName());
        Assert.assertEquals(project.getDescription(), actualProject.getDescription());
        Assert.assertEquals(project.getUserId(), actualProject.getUserId());
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByEmptyIndex() {
        projectService.findOneByIndex(null);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByNegativeIndex() {
        projectService.findOneByIndex(-2);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByIncorrectIndex() {
        projectService.findOneByIndex((int) projectService.getSize() + 1);
    }

    @Test
    public void testFindByIndexForUser() {
        @NotNull final ProjectDTO project = projectService.findAll(USER_ID_TWO).get(0);
        @Nullable final ProjectDTO actualProject = projectService.findOneByIndex(USER_ID_TWO, 0);
        Assert.assertNotNull(actualProject);
        Assert.assertEquals(project.getName(), actualProject.getName());
        Assert.assertEquals(project.getDescription(), actualProject.getDescription());
        Assert.assertEquals(project.getUserId(), actualProject.getUserId());
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByEmptyIndexForUser() {
        projectService.findOneByIndex(USER_ID_TWO, null);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByNegativeIndexForUser() {
        projectService.findOneByIndex(USER_ID_TWO, -2);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByIncorrectIndexForUser() {
        projectService.findOneByIndex(USER_ID_TWO, 12);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindByIndexForEmptyUser() {
        projectService.findOneByIndex("", 0);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindByIndexForNullUser() {
        projectService.findOneByIndex(null, 0);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByIndexForIncorrectUser() {
        projectService.findOneByIndex("some_id", 0);
    }

    @Test
    public void testGetSize() {
        final long expectedNumberOfEntries = projectService.getSize() + 1;
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName("Test Project");
        project.setUserId(USER_ID_ONE);
        projectService.add(project);
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize());
    }

    @Test
    public void testGetSizeForUser() {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName("Test Project");
        project.setUserId(USER_ID_TWO);
        projectService.add(project);
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2 + 1, projectService.getSize(USER_ID_TWO));
    }

    @Test(expected = UserIdEmptyException.class)
    public void testGetSizeForEmptyUser() {
        projectService.getSize("");
    }

    @Test(expected = UserIdEmptyException.class)
    public void testGetSizeForNullUser() {
        projectService.getSize(null);
    }

    @Test
    public void testGetSizeForIncorrectUser() {
        Assert.assertEquals(0, projectService.getSize("some_id"));
    }

    @Test
    public void testIsNotFoundById() {
        @NotNull final String validId = projectService.findAll().get(0).getId();
        @NotNull final String invalidId = UUID.randomUUID().toString();
        Assert.assertFalse(projectService.existsById(invalidId));
        Assert.assertTrue(projectService.existsById(validId));
    }

    @Test(expected = IdEmptyException.class)
    public void testIsNotFoundByEmptyId() {
        projectService.existsById("");
    }

    @Test(expected = IdEmptyException.class)
    public void testIsNotFoundByNullId() {
        projectService.existsById(null);
    }

    @Test
    public void testIsNotFoundByIdForUser() {
        @NotNull final String validId = projectService.findAll(USER_ID_ONE).get(0).getId();
        @NotNull final String invalidId = UUID.randomUUID().toString();
        Assert.assertFalse(projectService.existsById(USER_ID_ONE, invalidId));
        Assert.assertTrue(projectService.existsById(USER_ID_ONE, validId));
    }

    @Test(expected = IdEmptyException.class)
    public void testIsNotFoundByEmptyIdForUser() {
        projectService.existsById(USER_ID_ONE, "");
    }

    @Test(expected = IdEmptyException.class)
    public void testIsNotFoundByNullIdForUser() {
        projectService.existsById(USER_ID_ONE, null);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testIsNotFoundForEmptyUser() {
        projectService.existsById("", projectService.findAll(USER_ID_ONE).get(0).getId());
    }

    @Test(expected = UserIdEmptyException.class)
    public void testIsNotFoundForNullUser() {
        projectService.existsById(null, projectService.findAll(USER_ID_ONE).get(0).getId());
    }

    @Test
    public void testIsNotFoundForIncorrectUser() {
        Assert.assertFalse(projectService.existsById("some_id", projectService.findAll(USER_ID_ONE).get(0).getId()));
    }

    @Test
    public void testRemove() {
        final long expectedNumberOfEntries = projectService.getSize(USER_ID_ONE) - 1;
        final long expectedNumberOfTasks = taskService.getSize(USER_ID_ONE) - 1;
        @NotNull final ProjectDTO project = projectService.findAll(USER_ID_ONE).get(0);
        projectService.removeOne(project);
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize(USER_ID_ONE));
        Assert.assertEquals(expectedNumberOfTasks, taskService.getSize(USER_ID_ONE));
    }

    @Test(expected = ModelEmptyException.class)
    public void testRemoveNull() {
        projectService.removeOne(null);
    }

    @Test
    public void testRemoveById() {
        final long expectedNumberOfEntries = projectService.getSize() - 1;
        final long expectedNumberOfTasks = taskService.getSize() - 1;
        @NotNull final String projectId = projectService.findAll().get(0).getId();
        projectService.removeOneById(projectId);
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize());
        Assert.assertEquals(expectedNumberOfTasks, taskService.getSize());
    }

    @Test
    public void testRemoveByIdForUser() {
        final long expectedNumberOfEntries = projectService.getSize(USER_ID_ONE) - 1;
        final long expectedNumberOfTasks = taskService.getSize(USER_ID_ONE) - 1;
        @NotNull final String projectId = projectService.findAll(USER_ID_ONE).get(0).getId();
        projectService.removeOneById(USER_ID_ONE, projectId);
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize(USER_ID_ONE));
        Assert.assertEquals(expectedNumberOfTasks, taskService.getSize(USER_ID_ONE));
    }

    @Test(expected = IdEmptyException.class)
    public void testRemoveByEmptyId() {
        Assert.assertNull(projectService.removeOneById(""));
    }

    @Test(expected = IdEmptyException.class)
    public void testRemoveByNullId() {
        Assert.assertNull(projectService.removeOneById(null));
    }

    @Test(expected = IdEmptyException.class)
    public void testRemoveByEmptyIdForUser() {
        projectService.removeOneById(USER_ID_ONE, "");
    }

    @Test(expected = IdEmptyException.class)
    public void testRemoveByNullIdForUser() {
        projectService.removeOneById(USER_ID_ONE, null);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testRemoveByIdForEmptyUser() {
        @NotNull final String projectId = projectService.findAll().get(0).getId();
        projectService.removeOneById("", projectId);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testRemoveByIdForNullUser() {
        @NotNull final String projectId = projectService.findAll().get(0).getId();
        projectService.removeOneById(null, projectId);
    }

    @Test
    public void testRemoveByIdForIncorrectUser() {
        @NotNull final String projectId = projectService.findAll().get(0).getId();
        projectService.removeOneById("some_id", projectId);
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectService.getSize());
    }

    @Test
    public void testRemoveByIdProjectNotFound() {
        @NotNull final String invalidId = UUID.randomUUID().toString();
        Assert.assertNull(projectService.removeOneById(invalidId));
    }

    @Test
    public void testRemoveByIdProjectNotFoundForUser() {
        @NotNull final String invalidId = UUID.randomUUID().toString();
        Assert.assertNull(projectService.removeOneById(USER_ID_ONE, invalidId));
    }

    @Test
    public void testRemoveByIndex() {
        final long expectedNumberOfEntries = projectService.getSize() - 1;
        final long expectedNumberOfTasks = taskService.getSize() - 1;
        projectService.removeOneByIndex(0);
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize());
        Assert.assertEquals(expectedNumberOfTasks, taskService.getSize());
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByEmptyIndex() {
        projectService.removeOneByIndex(null);
    }

    @Test
    public void testRemoveByIndexForUser() {
        final long expectedNumberOfEntries = projectService.getSize(USER_ID_TWO) - 1;
        final long expectedNumberOfTasks = taskService.getSize(USER_ID_TWO) - 1;
        projectService.removeOneByIndex(USER_ID_TWO, 0);
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize(USER_ID_TWO));
        Assert.assertEquals(expectedNumberOfTasks, taskService.getSize(USER_ID_TWO));
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByEmptyIndexForUser() {
        projectService.removeOneByIndex(USER_ID_TWO, null);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testRemoveByIndexForEmptyUser() {
        projectService.removeOneByIndex("", 0);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testRemoveByIndexForNullUser() {
        projectService.removeOneByIndex(null, 0);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByIndexForIncorrectUser() {
        projectService.removeOneByIndex("some_id", 0);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByNegativeIndex() {
        projectService.removeOneByIndex(-2);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByNegativeIndexForUser() {
        projectService.removeOneByIndex(USER_ID_TWO, -2);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByIncorrectIndex() {
        projectService.removeOneByIndex((int) projectService.getSize() + 1);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByIncorrectIndexForUser() {
        projectService.removeOneByIndex(USER_ID_TWO, 5);
    }

    @After
    public void clearRepository() {
        userService.removeAll();
    }

}
