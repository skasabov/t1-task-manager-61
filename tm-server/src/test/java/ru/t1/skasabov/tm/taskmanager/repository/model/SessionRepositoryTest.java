package ru.t1.skasabov.tm.taskmanager.repository.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.t1.skasabov.tm.model.Session;
import ru.t1.skasabov.tm.model.User;
import ru.t1.skasabov.tm.repository.model.SessionRepository;
import ru.t1.skasabov.tm.repository.model.UserRepository;
import ru.t1.skasabov.tm.taskmanager.AbstractTest;

import java.util.List;
import java.util.UUID;

public class SessionRepositoryTest extends AbstractTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static User userOther;

    @NotNull
    private static String USER_ID_ONE;

    @NotNull
    private static String USER_ID_TWO;

    @NotNull
    private UserRepository userRepository;

    @NotNull
    private SessionRepository sessionRepository;

    @Before
    @SneakyThrows
    public void initRepository() {
        userRepository = context.getBean(UserRepository.class);
        @NotNull final User user = new User();
        user.setLogin("user_one");
        user.setPasswordHash("user_one");
        userOther = new User();
        userOther.setLogin("user_two");
        userOther.setPasswordHash("user_two");
        userRepository.save(user);
        userRepository.save(userOther);
        USER_ID_ONE = user.getId();
        USER_ID_TWO = userOther.getId();
        sessionRepository = context.getBean(SessionRepository.class);
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Session session = new Session();
            if (i <= 5) session.setUser(user);
            else session.setUser(userOther);
            sessionRepository.save(session);
        }
    }

    @Test
    public void testDeleteByUserId() {
        final long expectedNumberOfEntries = sessionRepository.count() - NUMBER_OF_ENTRIES / 2;
        sessionRepository.deleteByUserId(USER_ID_ONE);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.count());
    }

    @Test
    public void testDeleteByEmptyUserId() {
        sessionRepository.deleteByUserId("");
        Assert.assertEquals(NUMBER_OF_ENTRIES, sessionRepository.count());
    }

    @Test
    public void testDeleteByIncorrectUserId() {
        sessionRepository.deleteByUserId("some_id");
        Assert.assertEquals(NUMBER_OF_ENTRIES, sessionRepository.count());
    }

    @Test
    public void testFindByUserId() {
        @NotNull final List<Session> sessionList = sessionRepository.findByUserId(USER_ID_ONE);
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2, sessionList.size());
    }

    @Test
    public void testFindByEmptyUserId() {
        @NotNull final List<Session> sessionList = sessionRepository.findByUserId("");
        Assert.assertEquals(0, sessionList.size());
    }

    @Test
    public void testFindByIncorrectUserId() {
        @NotNull final List<Session> sessionList = sessionRepository.findByUserId("some_id");
        Assert.assertEquals(0, sessionList.size());
    }

    @Test
    public void testFindByUserIdAndId() {
        @NotNull final Session session = sessionRepository.findByUserId(USER_ID_ONE).get(0);
        @NotNull final String sessionId = sessionRepository.findByUserId(USER_ID_ONE).get(0).getId();
        @Nullable final Session actualSession = sessionRepository.findByUserIdAndId(USER_ID_ONE, sessionId);
        Assert.assertNotNull(actualSession);
        Assert.assertEquals(session.getDate(), actualSession.getDate());
        Assert.assertEquals(session.getRole(), actualSession.getRole());
        Assert.assertEquals(session.getUser().getId(), actualSession.getUser().getId());
    }

    @Test
    public void testFindByUserIdAndEmptyId() {
        Assert.assertNull(sessionRepository.findByUserIdAndId(USER_ID_ONE, ""));
    }

    @Test
    public void testFindByUserIdAndIncorrectId() {
        @NotNull final String id = UUID.randomUUID().toString();
        Assert.assertNull(sessionRepository.findByUserIdAndId(USER_ID_ONE, id));
    }

    @Test
    public void testFindByEmptyUserIdAndId() {
        @NotNull final String projectId = sessionRepository.findByUserId(USER_ID_ONE).get(0).getId();
        Assert.assertNull(sessionRepository.findByUserIdAndId("", projectId));
    }

    @Test
    public void testFindByIncorrectUserIdAndId() {
        @NotNull final String projectId = sessionRepository.findByUserId(USER_ID_ONE).get(0).getId();
        Assert.assertNull(sessionRepository.findByUserIdAndId("some_id", projectId));
    }

    @Test
    public void testFindByUserIdAndIndex() {
        @NotNull final Session session = sessionRepository.findByUserId(USER_ID_TWO).get(0);
        @Nullable final Session actualSession = sessionRepository.findOneByIndex(USER_ID_TWO, 0);
        Assert.assertNotNull(actualSession);
        Assert.assertEquals(session.getDate(), actualSession.getDate());
        Assert.assertEquals(session.getRole(), actualSession.getRole());
        Assert.assertEquals(session.getUser().getId(), actualSession.getUser().getId());
        Assert.assertNull(sessionRepository.findOneByIndex(USER_ID_TWO, NUMBER_OF_ENTRIES / 2));
        Assert.assertNull(sessionRepository.findOneByIndex("", NUMBER_OF_ENTRIES / 2));
        Assert.assertNull(sessionRepository.findOneByIndex("some_id", NUMBER_OF_ENTRIES / 2));
    }

    @Test
    public void testCountByUserId() {
        @NotNull final Session session = new Session();
        session.setUser(userOther);
        sessionRepository.save(session);
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2 + 1, sessionRepository.countByUserId(USER_ID_TWO));
        Assert.assertEquals(0, sessionRepository.countByUserId(""));
        Assert.assertEquals(0, sessionRepository.countByUserId("some_id"));
    }

    @Test
    public void testExistsByUserIdAndId() {
        @NotNull final String validId = sessionRepository.findByUserId(USER_ID_ONE).get(0).getId();
        @NotNull final String invalidId = UUID.randomUUID().toString();
        Assert.assertFalse(sessionRepository.existsByUserIdAndId("", validId));
        Assert.assertFalse(sessionRepository.existsByUserIdAndId("some_id", validId));
        Assert.assertFalse(sessionRepository.existsByUserIdAndId(USER_ID_ONE, ""));
        Assert.assertFalse(sessionRepository.existsByUserIdAndId(USER_ID_ONE, invalidId));
        Assert.assertTrue(sessionRepository.existsByUserIdAndId(USER_ID_ONE, validId));
    }

    @Test
    public void testDeleteByUserIdAndId() {
        final long expectedNumberOfEntries = sessionRepository.countByUserId(USER_ID_ONE) - 1;
        @NotNull final String sessionId = sessionRepository.findByUserId(USER_ID_ONE).get(0).getId();
        sessionRepository.deleteByUserIdAndId(USER_ID_ONE, sessionId);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.countByUserId(USER_ID_ONE));
    }

    @Test
    public void testDeleteByUserIdAndEmptyId() {
        sessionRepository.deleteByUserIdAndId(USER_ID_ONE, "");
        Assert.assertEquals(NUMBER_OF_ENTRIES, sessionRepository.count());
    }

    @Test
    public void testDeleteByUserIdAndIncorrectId() {
        sessionRepository.deleteByUserIdAndId(USER_ID_ONE, "some_id");
        Assert.assertEquals(NUMBER_OF_ENTRIES, sessionRepository.count());
    }

    @Test
    public void testDeleteByEmptyUserIdAndId() {
        @NotNull final String projectId = sessionRepository.findByUserId(USER_ID_ONE).get(0).getId();
        sessionRepository.deleteByUserIdAndId("", projectId);
        Assert.assertEquals(NUMBER_OF_ENTRIES, sessionRepository.count());
    }

    @Test
    public void testDeleteByIncorrectUserIdAndId() {
        @NotNull final String projectId = sessionRepository.findByUserId(USER_ID_ONE).get(0).getId();
        sessionRepository.deleteByUserIdAndId("some_id", projectId);
        Assert.assertEquals(NUMBER_OF_ENTRIES, sessionRepository.count());
    }

    @After
    @SneakyThrows
    public void clearRepository() {
        userRepository.deleteAll();
    }

}
