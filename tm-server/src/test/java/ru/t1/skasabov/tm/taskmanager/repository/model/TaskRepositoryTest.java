package ru.t1.skasabov.tm.taskmanager.repository.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.t1.skasabov.tm.enumerated.Status;
import ru.t1.skasabov.tm.model.Project;
import ru.t1.skasabov.tm.model.Task;
import ru.t1.skasabov.tm.model.User;
import ru.t1.skasabov.tm.repository.model.ProjectRepository;
import ru.t1.skasabov.tm.repository.model.TaskRepository;
import ru.t1.skasabov.tm.repository.model.UserRepository;
import ru.t1.skasabov.tm.taskmanager.AbstractTest;

import java.util.*;
import java.util.stream.Collectors;

public class TaskRepositoryTest extends AbstractTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static User userOther;

    @NotNull
    private static String USER_ID_ONE;

    @NotNull
    private static String USER_ID_TWO;

    @NotNull
    private Project projectOne;

    @NotNull
    private Project projectTwo;

    @NotNull
    private UserRepository userRepository;

    @NotNull
    private TaskRepository taskRepository;

    @Before
    @SneakyThrows
    public void initRepository() {
        final long currentTime = System.currentTimeMillis();
        userRepository = context.getBean(UserRepository.class);
        @NotNull final User user = new User();
        user.setLogin("user_one");
        user.setPasswordHash("user_one");
        userOther = new User();
        userOther.setLogin("user_two");
        userOther.setPasswordHash("user_two");
        userRepository.save(user);
        userRepository.save(userOther);
        USER_ID_ONE = user.getId();
        USER_ID_TWO = userOther.getId();
        @NotNull final ProjectRepository projectRepository = context.getBean(ProjectRepository.class);
        projectOne = new Project();
        projectOne.setUser(user);
        projectOne.setName("project_one");
        projectTwo = new Project();
        projectTwo.setUser(userOther);
        projectTwo.setName("project_two");
        projectRepository.save(projectOne);
        projectRepository.save(projectTwo);
        taskRepository = context.getBean(TaskRepository.class);
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Task task = new Task();
            task.setName("Test " + i);
            task.setCreated(new Date(currentTime + i * 1000));
            if (i < 4) task.setStatus(Status.COMPLETED);
            else if (i < 7) task.setStatus(Status.IN_PROGRESS);
            else task.setStatus(Status.NOT_STARTED);
            if (i <= 5) {
                task.setUser(user);
                task.setProject(projectOne);
            } else {
                task.setUser(userOther);
                task.setProject(projectTwo);
            }
            taskRepository.save(task);
        }
    }

    @Test
    public void testDeleteByUserId() {
        final long expectedNumberOfEntries = taskRepository.count() - NUMBER_OF_ENTRIES / 2;
        taskRepository.deleteByUserId(USER_ID_ONE);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.count());
    }

    @Test
    public void testDeleteByEmptyUserId() {
        taskRepository.deleteByUserId("");
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskRepository.count());
    }

    @Test
    public void testDeleteByIncorrectUserId() {
        taskRepository.deleteByUserId("some_id");
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskRepository.count());
    }

    @Test
    public void testFindByUserId() {
        @NotNull final List<Task> taskList = taskRepository.findByUserId(USER_ID_ONE);
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2, taskList.size());
    }

    @Test
    public void testFindByEmptyUserId() {
        @NotNull final List<Task> taskList = taskRepository.findByUserId("");
        Assert.assertEquals(0, taskList.size());
    }

    @Test
    public void testFindByIncorrectUserId() {
        @NotNull final List<Task> taskList = taskRepository.findByUserId("some_id");
        Assert.assertEquals(0, taskList.size());
    }

    @Test
    public void testFindAllSortByNameForUser() {
        @NotNull final List<Task> taskSortList = taskRepository.findAllSortByNameForUser(USER_ID_TWO);
        @NotNull final List<Task> actualTasks = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES / 2; i++) {
            @NotNull final Task task = new Task();
            task.setName("Test " + (i + 5));
            actualTasks.add(task);
        }
        actualTasks.add(0, actualTasks.get(NUMBER_OF_ENTRIES / 2 - 1));
        actualTasks.remove(NUMBER_OF_ENTRIES / 2);
        Assert.assertEquals(actualTasks, taskSortList);
    }

    @Test
    public void testFindAllSortByNameForEmptyUser() {
        @NotNull final List<Task> taskSortList = taskRepository.findAllSortByNameForUser("");
        Assert.assertEquals(Collections.emptyList(), taskSortList);
    }

    @Test
    public void testFindAllSortByNameForIncorrectUser() {
        @NotNull final List<Task> taskSortList = taskRepository.findAllSortByNameForUser("some_id");
        Assert.assertEquals(Collections.emptyList(), taskSortList);
    }

    @Test
    public void testFindAllSortByCreatedForUser() {
        @NotNull final List<Task> taskSortList = taskRepository.findAllSortByCreatedForUser(USER_ID_ONE);
        @NotNull final List<Task> actualTasks = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES / 2; i++) {
            @NotNull final Task task = new Task();
            task.setName("Test " + i);
            actualTasks.add(task);
        }
        Assert.assertEquals(actualTasks, taskSortList);
    }

    @Test
    public void testFindAllSortByCreatedForEmptyUser() {
        @NotNull final List<Task> taskSortList = taskRepository.findAllSortByCreatedForUser("");
        Assert.assertEquals(Collections.emptyList(), taskSortList);
    }

    @Test
    public void testFindAllSortByCreatedForIncorrectUser() {
        @NotNull final List<Task> taskSortList = taskRepository.findAllSortByCreatedForUser("some_id");
        Assert.assertEquals(Collections.emptyList(), taskSortList);
    }

    @Test
    public void testFindAllSortByStatusForUser() {
        @NotNull final List<Status> statusList = taskRepository.findAllSortByStatusForUser(USER_ID_TWO)
                .stream().map(Task::getStatus).collect(Collectors.toList());
        @NotNull final List<Status> actualStatuses = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES / 2; i++) {
            if (i < 2) actualStatuses.add(Status.IN_PROGRESS);
            else actualStatuses.add(Status.NOT_STARTED);
        }
        Assert.assertEquals(actualStatuses, statusList);
    }

    @Test
    public void testFindAllSortByStatusForEmptyUser() {
        @NotNull final List<Task> taskSortList = taskRepository.findAllSortByStatusForUser("");
        Assert.assertEquals(Collections.emptyList(), taskSortList);
    }

    @Test
    public void testFindAllSortByStatusForIncorrectUser() {
        @NotNull final List<Task> taskSortList = taskRepository.findAllSortByStatusForUser("some_id");
        Assert.assertEquals(Collections.emptyList(), taskSortList);
    }

    @Test
    public void testFindByUserIdAndId() {
        @NotNull final Task task = taskRepository.findByUserId(USER_ID_ONE).get(0);
        @NotNull final String taskId = taskRepository.findByUserId(USER_ID_ONE).get(0).getId();
        @Nullable final Task actualTask = taskRepository.findByUserIdAndId(USER_ID_ONE, taskId);
        Assert.assertNotNull(actualTask);
        Assert.assertEquals(task.getName(), actualTask.getName());
        Assert.assertEquals(task.getDescription(), actualTask.getDescription());
        Assert.assertEquals(task.getUser().getId(), actualTask.getUser().getId());
    }

    @Test
    public void testFindByUserIdAndEmptyId() {
        Assert.assertNull(taskRepository.findByUserIdAndId(USER_ID_ONE, ""));
    }

    @Test
    public void testFindByUserIdAndIncorrectId() {
        @NotNull final String id = UUID.randomUUID().toString();
        Assert.assertNull(taskRepository.findByUserIdAndId(USER_ID_ONE, id));
    }

    @Test
    public void testFindByEmptyUserIdAndId() {
        @NotNull final String projectId = taskRepository.findByUserId(USER_ID_ONE).get(0).getId();
        Assert.assertNull(taskRepository.findByUserIdAndId("", projectId));
    }

    @Test
    public void testFindByIncorrectUserIdAndId() {
        @NotNull final String projectId = taskRepository.findByUserId(USER_ID_ONE).get(0).getId();
        Assert.assertNull(taskRepository.findByUserIdAndId("some_id", projectId));
    }

    @Test
    public void testFindByUserIdAndIndex() {
        @NotNull final Task task = taskRepository.findByUserId(USER_ID_TWO).get(0);
        @Nullable final Task actualTask = taskRepository.findOneByIndex(USER_ID_TWO, 0);
        Assert.assertNotNull(actualTask);
        Assert.assertEquals(task.getName(), actualTask.getName());
        Assert.assertEquals(task.getDescription(), actualTask.getDescription());
        Assert.assertEquals(task.getUser().getId(), actualTask.getUser().getId());
        Assert.assertNull(taskRepository.findOneByIndex(USER_ID_TWO, NUMBER_OF_ENTRIES / 2));
        Assert.assertNull(taskRepository.findOneByIndex("", NUMBER_OF_ENTRIES / 2));
        Assert.assertNull(taskRepository.findOneByIndex("some_id", NUMBER_OF_ENTRIES / 2));
    }

    @Test
    public void testCountByUserId() {
        @NotNull final Task task = new Task();
        task.setName("Test Task");
        task.setUser(userOther);
        taskRepository.save(task);
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2 + 1, taskRepository.countByUserId(USER_ID_TWO));
        Assert.assertEquals(0, taskRepository.countByUserId(""));
        Assert.assertEquals(0, taskRepository.countByUserId("some_id"));
    }

    @Test
    public void testExistsByUserIdAndId() {
        @NotNull final String validId = taskRepository.findByUserId(USER_ID_ONE).get(0).getId();
        @NotNull final String invalidId = UUID.randomUUID().toString();
        Assert.assertFalse(taskRepository.existsByUserIdAndId("", validId));
        Assert.assertFalse(taskRepository.existsByUserIdAndId("some_id", validId));
        Assert.assertFalse(taskRepository.existsByUserIdAndId(USER_ID_ONE, ""));
        Assert.assertFalse(taskRepository.existsByUserIdAndId(USER_ID_ONE, invalidId));
        Assert.assertTrue(taskRepository.existsByUserIdAndId(USER_ID_ONE, validId));
    }

    @Test
    public void testDeleteByUserIdAndId() {
        final long expectedNumberOfEntries = taskRepository.countByUserId(USER_ID_ONE) - 1;
        @NotNull final String taskId = taskRepository.findByUserId(USER_ID_ONE).get(0).getId();
        taskRepository.deleteByUserIdAndId(USER_ID_ONE, taskId);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.countByUserId(USER_ID_ONE));
    }
    @Test
    public void testDeleteByUserIdAndEmptyId() {
        taskRepository.deleteByUserIdAndId(USER_ID_ONE, "");
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskRepository.count());
    }

    @Test
    public void testDeleteByUserIdAndIncorrectId() {
        taskRepository.deleteByUserIdAndId(USER_ID_ONE, "some_id");
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskRepository.count());
    }

    @Test
    public void testDeleteByEmptyUserIdAndId() {
        @NotNull final String projectId = taskRepository.findByUserId(USER_ID_ONE).get(0).getId();
        taskRepository.deleteByUserIdAndId("", projectId);
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskRepository.count());
    }

    @Test
    public void testDeleteByIncorrectUserIdAndId() {
        @NotNull final String projectId = taskRepository.findByUserId(USER_ID_ONE).get(0).getId();
        taskRepository.deleteByUserIdAndId("some_id", projectId);
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskRepository.count());
    }

    @Test
    public void testFindByUserIdAndProjectId() {
        @NotNull final List<Task> tasksOne = taskRepository.findByUserId(USER_ID_ONE);
        @NotNull final List<Task> tasks = taskRepository.findByUserIdAndProjectId(USER_ID_ONE, projectOne.getId());
        Assert.assertEquals(tasksOne, tasks);
    }

    @Test
    public void testFindByUserIdAndEmptyProjectId() {
        @NotNull final List<Task> tasks = taskRepository.findByUserIdAndProjectId(USER_ID_ONE, "");
        Assert.assertEquals(Collections.emptyList(), tasks);
    }

    @Test
    public void testFindByUserIdAndIncorrectProjectId() {
        @NotNull final List<Task> tasks = taskRepository.findByUserIdAndProjectId(USER_ID_ONE, projectTwo.getId());
        Assert.assertEquals(Collections.emptyList(), tasks);
    }

    @Test
    public void testFindByEmptyUserIdAndProjectId() {
        @NotNull final List<Task> tasks = taskRepository.findByUserIdAndProjectId("", projectTwo.getId());
        Assert.assertEquals(Collections.emptyList(), tasks);
    }

    @Test
    public void testFindByIncorrectUserIdAndProjectId() {
        @NotNull final List<Task> tasks = taskRepository.findByUserIdAndProjectId("some_id", projectTwo.getId());
        Assert.assertEquals(Collections.emptyList(), tasks);
    }

    @After
    @SneakyThrows
    public void clearRepository() {
        userRepository.deleteAll();
    }

}
