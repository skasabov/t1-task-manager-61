package ru.t1.skasabov.tm.taskmanager.repository.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.t1.skasabov.tm.dto.model.ProjectDTO;
import ru.t1.skasabov.tm.dto.model.TaskDTO;
import ru.t1.skasabov.tm.dto.model.UserDTO;
import ru.t1.skasabov.tm.enumerated.Status;
import ru.t1.skasabov.tm.repository.dto.ProjectDTORepository;
import ru.t1.skasabov.tm.repository.dto.TaskDTORepository;
import ru.t1.skasabov.tm.repository.dto.UserDTORepository;
import ru.t1.skasabov.tm.taskmanager.AbstractTest;

import java.util.*;
import java.util.stream.Collectors;

public class ProjectDTORepositoryTest extends AbstractTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static UserDTO userOne;

    @NotNull
    private static UserDTO userTwo;

    @NotNull
    private static String USER_ID_ONE;

    @NotNull
    private static String USER_ID_TWO;

    @NotNull
    private TaskDTORepository taskRepository;

    @NotNull
    private ProjectDTORepository projectRepository;

    @NotNull
    private UserDTORepository userRepository;

    @Before
    @SneakyThrows
    public void initRepository() {
        userRepository = context.getBean(UserDTORepository.class);
        userOne = new UserDTO();
        userOne.setLogin("user_one");
        userOne.setPasswordHash("user_one");
        userTwo = new UserDTO();
        userTwo.setLogin("user_two");
        userTwo.setPasswordHash("user_two");
        userRepository.save(userOne);
        userRepository.save(userTwo);
        USER_ID_ONE = userOne.getId();
        USER_ID_TWO = userTwo.getId();
        final long currentTime = System.currentTimeMillis();
        @NotNull final List<TaskDTO> tasks = new ArrayList<>();
        projectRepository = context.getBean(ProjectDTORepository.class);
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final ProjectDTO project = new ProjectDTO();
            project.setName("Test " + i);
            project.setCreated(new Date(currentTime + i * 1000));
            if (i < 4) project.setStatus(Status.COMPLETED);
            else if (i < 7) project.setStatus(Status.IN_PROGRESS);
            else project.setStatus(Status.NOT_STARTED);
            if (i <= 5) project.setUserId(USER_ID_ONE);
            else project.setUserId(USER_ID_TWO);
            projectRepository.save(project);
            @NotNull final TaskDTO task = new TaskDTO();
            task.setName("Test " + i);
            if (i <= 5) task.setUserId(USER_ID_ONE);
            else task.setUserId(USER_ID_TWO);
            task.setProjectId(project.getId());
            tasks.add(task);
        }
        taskRepository = context.getBean(TaskDTORepository.class);
        taskRepository.saveAll(tasks);
    }

    @Test
    public void testDeleteByUserId() {
        final long expectedNumberOfEntries = projectRepository.count() - NUMBER_OF_ENTRIES / 2;
        final long expectedNumberOfTasks = taskRepository.count() - NUMBER_OF_ENTRIES / 2;
        taskRepository.deleteAll(taskRepository.findAllByProjects(USER_ID_ONE));
        projectRepository.deleteByUserId(USER_ID_ONE);
        Assert.assertEquals(expectedNumberOfTasks, taskRepository.count());
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.count());
    }

    @Test
    public void testDeleteByEmptyUserId() {
        projectRepository.deleteByUserId("");
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectRepository.count());
    }

    @Test
    public void testDeleteByIncorrectUserId() {
        projectRepository.deleteByUserId("some_id");
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectRepository.count());
    }

    @Test
    public void testFindByUserId() {
        @NotNull final List<ProjectDTO> projectList = projectRepository.findByUserId(USER_ID_ONE);
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2, projectList.size());
    }

    @Test
    public void testFindByEmptyUserId() {
        @NotNull final List<ProjectDTO> projectList = projectRepository.findByUserId("");
        Assert.assertEquals(0, projectList.size());
    }

    @Test
    public void testFindByIncorrectUserId() {
        @NotNull final List<ProjectDTO> projectList = projectRepository.findByUserId("some_id");
        Assert.assertEquals(0, projectList.size());
    }

    @Test
    public void testFindAllSortByNameForUser() {
        @NotNull final List<ProjectDTO> projectSortList = projectRepository.findAllSortByNameForUser(USER_ID_TWO);
        @NotNull final List<ProjectDTO> actualProjects = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES / 2; i++) {
            @NotNull final ProjectDTO project = new ProjectDTO();
            project.setName("Test " + (i + 5));
            actualProjects.add(project);
        }
        actualProjects.add(0, actualProjects.get(NUMBER_OF_ENTRIES / 2 - 1));
        actualProjects.remove(NUMBER_OF_ENTRIES / 2);
        Assert.assertEquals(actualProjects, projectSortList);
    }

    @Test
    public void testFindAllSortByNameForEmptyUser() {
        @NotNull final List<ProjectDTO> projectSortList = projectRepository.findAllSortByNameForUser("");
        Assert.assertEquals(Collections.emptyList(), projectSortList);
    }

    @Test
    public void testFindAllSortByNameForIncorrectUser() {
        @NotNull final List<ProjectDTO> projectSortList = projectRepository.findAllSortByNameForUser("some_id");
        Assert.assertEquals(Collections.emptyList(), projectSortList);
    }

    @Test
    public void testFindAllSortByCreatedForUser() {
        @NotNull final List<ProjectDTO> projectSortList = projectRepository.findAllSortByCreatedForUser(USER_ID_ONE);
        @NotNull final List<ProjectDTO> actualProjects = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES / 2; i++) {
            @NotNull final ProjectDTO project = new ProjectDTO();
            project.setName("Test " + i);
            actualProjects.add(project);
        }
        Assert.assertEquals(actualProjects, projectSortList);
    }

    @Test
    public void testFindAllSortByCreatedForEmptyUser() {
        @NotNull final List<ProjectDTO> projectSortList = projectRepository.findAllSortByCreatedForUser("");
        Assert.assertEquals(Collections.emptyList(), projectSortList);
    }

    @Test
    public void testFindAllSortByCreatedForIncorrectUser() {
        @NotNull final List<ProjectDTO> projectSortList = projectRepository.findAllSortByCreatedForUser("some_id");
        Assert.assertEquals(Collections.emptyList(), projectSortList);
    }

    @Test
    public void testFindAllSortByStatusForUser() {
        @NotNull final List<Status> statusList = projectRepository.findAllSortByStatusForUser(USER_ID_TWO)
                .stream().map(ProjectDTO::getStatus).collect(Collectors.toList());
        @NotNull final List<Status> actualStatuses = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES / 2; i++) {
            if (i < 2) actualStatuses.add(Status.IN_PROGRESS);
            else actualStatuses.add(Status.NOT_STARTED);
        }
        Assert.assertEquals(actualStatuses, statusList);
    }

    @Test
    public void testFindAllSortByStatusForEmptyUser() {
        @NotNull final List<ProjectDTO> projectSortList = projectRepository.findAllSortByStatusForUser("");
        Assert.assertEquals(Collections.emptyList(), projectSortList);
    }

    @Test
    public void testFindAllSortByStatusForIncorrectUser() {
        @NotNull final List<ProjectDTO> projectSortList = projectRepository.findAllSortByStatusForUser("some_id");
        Assert.assertEquals(Collections.emptyList(), projectSortList);
    }

    @Test
    public void testFindByUserIdAndId() {
        @NotNull final ProjectDTO project = projectRepository.findByUserId(USER_ID_ONE).get(0);
        @NotNull final String projectId = projectRepository.findByUserId(USER_ID_ONE).get(0).getId();
        @Nullable final ProjectDTO actualProject = projectRepository.findByUserIdAndId(USER_ID_ONE, projectId);
        Assert.assertNotNull(actualProject);
        Assert.assertEquals(project.getName(), actualProject.getName());
        Assert.assertEquals(project.getDescription(), actualProject.getDescription());
        Assert.assertEquals(project.getUserId(), actualProject.getUserId());
    }

    @Test
    public void testFindByUserIdAndEmptyId() {
        Assert.assertNull(projectRepository.findByUserIdAndId(USER_ID_ONE, ""));
    }

    @Test
    public void testFindByUserIdAndIncorrectId() {
        @NotNull final String id = UUID.randomUUID().toString();
        Assert.assertNull(projectRepository.findByUserIdAndId(USER_ID_ONE, id));
    }

    @Test
    public void testFindByEmptyUserIdAndId() {
        @NotNull final String projectId = projectRepository.findByUserId(USER_ID_ONE).get(0).getId();
        Assert.assertNull(projectRepository.findByUserIdAndId("", projectId));
    }

    @Test
    public void testFindByIncorrectUserIdAndId() {
        @NotNull final String projectId = projectRepository.findByUserId(USER_ID_ONE).get(0).getId();
        Assert.assertNull(projectRepository.findByUserIdAndId("some_id", projectId));
    }

    @Test
    public void testFindByUserIdAndIndex() {
        @NotNull final ProjectDTO project = projectRepository.findByUserId(USER_ID_TWO).get(0);
        @Nullable final ProjectDTO actualProject = projectRepository.findOneByIndex(USER_ID_TWO, 0);
        Assert.assertNotNull(actualProject);
        Assert.assertEquals(project.getName(), actualProject.getName());
        Assert.assertEquals(project.getDescription(), actualProject.getDescription());
        Assert.assertEquals(project.getUserId(), actualProject.getUserId());
        Assert.assertNull(projectRepository.findOneByIndex(USER_ID_TWO, NUMBER_OF_ENTRIES / 2));
        Assert.assertNull(projectRepository.findOneByIndex("", NUMBER_OF_ENTRIES / 2));
        Assert.assertNull(projectRepository.findOneByIndex("some_id", NUMBER_OF_ENTRIES / 2));
    }

    @Test
    public void testCountByUserId() {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName("Test Project");
        project.setUserId(USER_ID_TWO);
        projectRepository.save(project);
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2 + 1, projectRepository.countByUserId(USER_ID_TWO));
        Assert.assertEquals(0, projectRepository.countByUserId(""));
        Assert.assertEquals(0, projectRepository.countByUserId("some_id"));
    }

    @Test
    public void testExistsByUserIdAndId() {
        @NotNull final String validId = projectRepository.findByUserId(USER_ID_ONE).get(0).getId();
        @NotNull final String invalidId = UUID.randomUUID().toString();
        Assert.assertFalse(projectRepository.existsByUserIdAndId("", validId));
        Assert.assertFalse(projectRepository.existsByUserIdAndId("some_id", validId));
        Assert.assertFalse(projectRepository.existsByUserIdAndId(USER_ID_ONE, ""));
        Assert.assertFalse(projectRepository.existsByUserIdAndId(USER_ID_ONE, invalidId));
        Assert.assertTrue(projectRepository.existsByUserIdAndId(USER_ID_ONE, validId));
    }

    @Test
    public void testDeleteByUserIdAndId() {
        final long expectedNumberOfEntries = projectRepository.countByUserId(USER_ID_ONE) - 1;
        @NotNull final String projectId = projectRepository.findByUserId(USER_ID_ONE).get(0).getId();
        final long expectedNumberOfTasks = taskRepository.countByUserId(USER_ID_ONE) - 1;
        taskRepository.deleteAll(taskRepository.findByProjectId(projectId));
        projectRepository.deleteByUserIdAndId(USER_ID_ONE, projectId);
        Assert.assertEquals(expectedNumberOfTasks, taskRepository.countByUserId(USER_ID_ONE));
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.countByUserId(USER_ID_ONE));
    }

    @Test
    public void testDeleteByUserIdAndEmptyId() {
        projectRepository.deleteByUserIdAndId(USER_ID_ONE, "");
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectRepository.count());
    }

    @Test
    public void testDeleteByUserIdAndIncorrectId() {
        projectRepository.deleteByUserIdAndId(USER_ID_ONE, "some_id");
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectRepository.count());
    }

    @Test
    public void testDeleteByEmptyUserIdAndId() {
        @NotNull final String projectId = projectRepository.findByUserId(USER_ID_ONE).get(0).getId();
        projectRepository.deleteByUserIdAndId("", projectId);
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectRepository.count());
    }

    @Test
    public void testDeleteByIncorrectUserIdAndId() {
        @NotNull final String projectId = projectRepository.findByUserId(USER_ID_ONE).get(0).getId();
        projectRepository.deleteByUserIdAndId("some_id", projectId);
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectRepository.count());
    }

    @Test
    public void testFindAllByUsers() {
        @NotNull final List<String> users = new ArrayList<>(Arrays.asList(userOne.getId(), userTwo.getId()));
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectRepository.findAllByUsers(users).size());
    }

    @After
    @SneakyThrows
    public void clearRepository() {
        taskRepository.deleteAll();
        projectRepository.deleteAll();
        userRepository.deleteAll();
    }

}
