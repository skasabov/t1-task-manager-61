package ru.t1.skasabov.tm.taskmanager.repository.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.t1.skasabov.tm.api.service.IPropertyService;
import ru.t1.skasabov.tm.dto.model.UserDTO;
import ru.t1.skasabov.tm.repository.dto.UserDTORepository;
import ru.t1.skasabov.tm.taskmanager.AbstractTest;
import ru.t1.skasabov.tm.util.HashUtil;

public class UserDTORepositoryTest extends AbstractTest {

    @NotNull
    private UserDTO cat;

    @NotNull
    private UserDTO mouse;

    @NotNull
    private UserDTORepository userRepository;

    @Before
    @SneakyThrows
    public void initRepository() {
        @NotNull final IPropertyService propertyService = context.getBean(IPropertyService.class);
        userRepository = context.getBean(UserDTORepository.class);
        cat = new UserDTO();
        cat.setLogin("cat");
        @Nullable final String passwordHashCat = HashUtil.salt(propertyService, "cat");
        Assert.assertNotNull(passwordHashCat);
        cat.setPasswordHash(passwordHashCat);
        cat.setEmail("cat@cat");
        mouse = new UserDTO();
        mouse.setLogin("mouse");
        @Nullable final String passwordHashMouse = HashUtil.salt(propertyService, "mouse");
        Assert.assertNotNull(passwordHashMouse);
        mouse.setPasswordHash(passwordHashMouse);
        mouse.setEmail("mouse@mouse");
        userRepository.save(cat);
        userRepository.save(mouse);
    }

    @Test
    public void testFindByLogin() {
        @Nullable final UserDTO actualUser = userRepository.findByLogin("cat");
        Assert.assertNotNull(actualUser);
        Assert.assertEquals(cat.getLogin(), actualUser.getLogin());
        Assert.assertEquals(cat.getEmail(), actualUser.getEmail());
        Assert.assertEquals(cat.getPasswordHash(), actualUser.getPasswordHash());
        Assert.assertEquals(cat.getRole(), actualUser.getRole());
    }

    @Test
    public void testFindByEmptyLogin() {
        Assert.assertNull(userRepository.findByLogin(""));
    }

    @Test
    public void testFindByIncorrectLogin() {
        Assert.assertNull(userRepository.findByLogin("dog"));
    }

    @Test
    public void testFindByEmail() {
        @Nullable final UserDTO actualUser = userRepository.findByEmail("mouse@mouse");
        Assert.assertNotNull(actualUser);
        Assert.assertEquals(mouse.getLogin(), actualUser.getLogin());
        Assert.assertEquals(mouse.getEmail(), actualUser.getEmail());
        Assert.assertEquals(mouse.getPasswordHash(), actualUser.getPasswordHash());
        Assert.assertEquals(mouse.getRole(), actualUser.getRole());
    }

    @Test
    public void testFindByEmptyEmail() {
        Assert.assertNull(userRepository.findByEmail(""));
    }

    @Test
    public void testFindByIncorrectEmail() {
        Assert.assertNull(userRepository.findByEmail("dog@dog"));
    }

    @Test
    public void testExistsByLogin() {
        Assert.assertTrue(userRepository.existsByLogin("cat"));
    }

    @Test
    public void testExistsByEmptyLogin() {
        Assert.assertFalse(userRepository.existsByLogin(""));
    }

    @Test
    public void testExistsByIncorrectLogin() {
        Assert.assertFalse(userRepository.existsByLogin("dog"));
    }

    @Test
    public void testExistsByEmail() {
        Assert.assertTrue(userRepository.existsByEmail("cat@cat"));
    }

    @Test
    public void testExistsByEmptyEmail() {
        Assert.assertFalse(userRepository.existsByEmail(""));
    }

    @Test
    public void testExistsByIncorrectEmail() {
        Assert.assertFalse(userRepository.existsByLogin("dog@dog"));
    }

    @After
    @SneakyThrows
    public void clearRepository() {
        userRepository.deleteAll();
    }

}
