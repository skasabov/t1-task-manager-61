package ru.t1.skasabov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.skasabov.tm.model.User;

@Repository
public interface UserRepository extends AbstractRepository<User> {

    @Nullable
    User findByLogin(@NotNull @Param("login") String login);

    @Nullable
    User findByEmail(@NotNull @Param("email") String email);

    @NotNull
    Boolean existsByLogin(@NotNull @Param("login") String login);

    @NotNull
    Boolean existsByEmail(@NotNull @Param("email") String email);

}
