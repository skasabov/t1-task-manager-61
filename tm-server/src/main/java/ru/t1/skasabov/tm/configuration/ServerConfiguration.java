package ru.t1.skasabov.tm.configuration;

import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.resource.ClassLoaderResourceAccessor;
import lombok.SneakyThrows;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import ru.t1.skasabov.tm.api.service.IPropertyService;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@ComponentScan("ru.t1.skasabov.tm")
@EnableJpaRepositories("ru.t1.skasabov.tm.repository")
public class ServerConfiguration {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Bean
    @NotNull
    public DataSource dataSource() {
        @NotNull final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(propertyService.getDatabaseDriver());
        dataSource.setUrl(propertyService.getDatabaseUrl());
        dataSource.setUsername(propertyService.getDatabaseUserName());
        dataSource.setPassword(propertyService.getDatabasePassword());
        return dataSource;
    }

    @Bean
    @NotNull
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(@NotNull final DataSource dataSource) {
        @NotNull final LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan("ru.t1.skasabov.tm.dto.model", "ru.t1.skasabov.tm.model");
        Properties properties = new Properties();
        properties.put(Environment.SHOW_SQL, propertyService.getDatabaseShowSql());
        properties.put(Environment.HBM2DDL_AUTO, propertyService.getDatabaseHbm2ddlAuto());
        properties.put(Environment.DIALECT, propertyService.getDatabaseDialect());
        properties.put(Environment.USE_SECOND_LEVEL_CACHE, propertyService.getDatabaseCacheUseSecondLevelCache());
        properties.put(Environment.USE_QUERY_CACHE, propertyService.getDatabaseCacheUseQueryCache());
        properties.put(Environment.USE_MINIMAL_PUTS, propertyService.getDatabaseCacheUseMinimalPuts());
        properties.put(Environment.CACHE_REGION_PREFIX, propertyService.getDatabaseCacheRegionPrefix());
        properties.put(Environment.CACHE_PROVIDER_CONFIG, propertyService.getDatabaseCacheProviderConfigurationFileResourcePath());
        properties.put(Environment.CACHE_REGION_FACTORY, propertyService.getDatabaseCacheRegionFactoryClass());
        factoryBean.setJpaProperties(properties);
        return factoryBean;
    }

    @Bean
    @NotNull
    public PlatformTransactionManager transactionManager(@NotNull final LocalContainerEntityManagerFactoryBean entityManagerFactory) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory.getObject());
        return transactionManager;
    }

    @Bean
    @NotNull
    @SneakyThrows
    @Scope("prototype")
    public Liquibase liquibase() {
        @NotNull final Connection connection = DriverManager.getConnection(
                propertyService.getDatabaseUrl(),
                propertyService.getDatabaseUserName(),
                propertyService.getDatabasePassword()
        );
        @NotNull final JdbcConnection jdbcConnection = new JdbcConnection(connection);
        @NotNull final Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(jdbcConnection);
        @NotNull final ClassLoaderResourceAccessor accessor = new ClassLoaderResourceAccessor();
        return new Liquibase("changelog/changelog-master.xml", accessor, database);
    }

}
