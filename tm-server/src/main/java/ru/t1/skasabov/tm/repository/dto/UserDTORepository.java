package ru.t1.skasabov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.skasabov.tm.dto.model.UserDTO;

@Repository
public interface UserDTORepository extends AbstractDTORepository<UserDTO> {

    @Nullable
    UserDTO findByLogin(@NotNull @Param("login") String login);

    @Nullable
    UserDTO findByEmail(@NotNull @Param("email") String email);

    @NotNull
    Boolean existsByLogin(@NotNull @Param("login") String login);

    @NotNull
    Boolean existsByEmail(@NotNull @Param("email") String email);

}
