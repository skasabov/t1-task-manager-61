package ru.t1.skasabov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.skasabov.tm.model.Task;

import java.util.List;

@Repository
public interface TaskRepository extends AbstractUserOwnedRepository<Task> {

    @NotNull
    @Query("SELECT t FROM Task t WHERE t.user.id = :userId ORDER BY t.created")
    List<Task> findAllSortByCreatedForUser(@NotNull @Param("userId") String userId);

    @NotNull
    @Query("SELECT t FROM Task t WHERE t.user.id = :userId ORDER BY t.status")
    List<Task> findAllSortByStatusForUser(@NotNull @Param("userId") String userId);

    @NotNull
    @Query("SELECT t FROM Task t WHERE t.user.id = :userId ORDER BY t.name")
    List<Task> findAllSortByNameForUser(@NotNull @Param("userId") String userId);

    @Nullable
    @Query(value = "SELECT * FROM tm_task WHERE user_id = :userId LIMIT 1 OFFSET :index", nativeQuery = true)
    Task findOneByIndex(@NotNull @Param("userId") String userId, @NotNull @Param("index") Integer index);

    @NotNull
    List<Task> findByUserIdAndProjectId(@NotNull @Param("userId") String userId, @NotNull @Param("projectId") String projectId);

}
