package ru.t1.skasabov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.skasabov.tm.model.Session;

@Repository
public interface SessionRepository extends AbstractUserOwnedRepository<Session> {

    @Nullable
    @Query(value = "SELECT * FROM tm_session WHERE user_id = :userId LIMIT 1 OFFSET :index", nativeQuery = true)
    Session findOneByIndex(@NotNull @Param("userId") String userId, @NotNull @Param("index") Integer index);

}
