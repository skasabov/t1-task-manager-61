package ru.t1.skasabov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.dto.model.TaskDTO;

public interface IProjectTaskDTOService {

    @NotNull
    TaskDTO bindTaskToProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    @NotNull
    TaskDTO unbindTaskFromProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

}
