package ru.t1.skasabov.tm.service.model;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.skasabov.tm.api.service.model.ISessionService;
import ru.t1.skasabov.tm.exception.entity.ModelEmptyException;
import ru.t1.skasabov.tm.exception.field.IdEmptyException;
import ru.t1.skasabov.tm.exception.field.IndexIncorrectException;
import ru.t1.skasabov.tm.exception.field.UserIdEmptyException;
import ru.t1.skasabov.tm.model.AbstractModel;
import ru.t1.skasabov.tm.model.Session;
import ru.t1.skasabov.tm.repository.model.SessionRepository;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
@NoArgsConstructor
public class SessionService extends AbstractUserOwnedService<Session> implements ISessionService {

    @NotNull
    @Autowired
    private SessionRepository repository;

    @Override
    @SneakyThrows
    @Transactional
    public void removeAll(@Nullable final Collection<Session> collection) {
        if (collection == null) return;
        @NotNull final List<String> sessionIds = collection.stream().map(AbstractModel::getId).collect(Collectors.toList());
        @NotNull final List<Session> sessions = repository.findAllById(sessionIds);
        repository.deleteAll(sessions);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Collection<Session> addAll(@Nullable final Collection<Session> models) {
        if (models == null) return Collections.emptyList();
        repository.saveAll(models);
        return models;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Collection<Session> set(@Nullable final Collection<Session> models) {
        if (models == null) return Collections.emptyList();
        repository.deleteAll();
        repository.saveAll(models);
        return models;
    }

    @NotNull
    @Override
    @Transactional
    public Session add(@Nullable final Session model) {
        if (model == null) throw new ModelEmptyException();
        repository.save(model);
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Session> findAll() {
        return repository.findAll();
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findById(id).orElse(null);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session findOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize()) throw new IndexIncorrectException();
        return repository.findAll(PageRequest.of(index, 1)).getContent().get(0);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Session removeOne(@Nullable final Session model) {
        if (model == null) throw new ModelEmptyException();
        @Nullable final Session session = repository.findById(model.getId()).orElse(null);
        if (session == null) throw new ModelEmptyException();
        repository.delete(session);
        return session;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Session removeOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Session model = findOneById(id);
        if (model == null) return null;
        repository.deleteById(id);
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Session removeOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize()) throw new IndexIncorrectException();
        @Nullable final Session model = findOneByIndex(index);
        if (model == null) return null;
        repository.delete(model);
        return model;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeAll() {
        repository.deleteAll();
    }

    @Override
    @SneakyThrows
    public long getSize() {
        return repository.count();
    }

    @NotNull
    @Override
    @SneakyThrows
    public Boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existsById(id);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.deleteByUserId(userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existsByUserIdAndId(userId, id);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Session> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findByUserId(userId);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findByUserIdAndId(userId, id);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        return repository.findOneByIndex(userId, index);
    }

    @Override
    @SneakyThrows
    public long getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.countByUserId(userId);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Session removeOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Session model = findOneById(userId, id);
        if (model == null) return null;
        repository.deleteByUserIdAndId(userId, id);
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Session removeOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        @Nullable final Session model = findOneByIndex(userId, index);
        if (model == null) return null;
        repository.delete(model);
        return model;
    }

}
