package ru.t1.skasabov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.skasabov.tm.dto.model.SessionDTO;

import java.util.Collection;
import java.util.List;

@Repository
public interface SessionDTORepository extends AbstractUserOwnedDTORepository<SessionDTO> {

    @Nullable
    @Query(value = "SELECT * FROM tm_session WHERE user_id = :userId LIMIT 1 OFFSET :index", nativeQuery = true)
    SessionDTO findOneByIndex(@NotNull @Param("userId") String userId, @NotNull @Param("index") Integer index);

    @NotNull
    @Query("SELECT s FROM SessionDTO s INNER JOIN FETCH UserDTO u ON s.userId = u.id WHERE u.id IN :users")
    List<SessionDTO> findAllByUsers(@NotNull @Param("users") Collection<String> collection);

}
