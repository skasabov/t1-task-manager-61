package ru.t1.skasabov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.skasabov.tm.dto.model.TaskDTO;

import java.util.Collection;
import java.util.List;

@Repository
public interface TaskDTORepository extends AbstractUserOwnedDTORepository<TaskDTO> {

    @NotNull
    @Query("SELECT t FROM TaskDTO t WHERE t.userId = :userId ORDER BY t.created")
    List<TaskDTO> findAllSortByCreatedForUser(@NotNull @Param("userId") String userId);

    @NotNull
    @Query("SELECT t FROM TaskDTO t WHERE t.userId = :userId ORDER BY t.status")
    List<TaskDTO> findAllSortByStatusForUser(@NotNull @Param("userId") String userId);

    @NotNull
    @Query("SELECT t FROM TaskDTO t WHERE t.userId = :userId ORDER BY t.name")
    List<TaskDTO> findAllSortByNameForUser(@NotNull @Param("userId") String userId);

    @Nullable
    @Query(value = "SELECT * FROM tm_task WHERE user_id = :userId LIMIT 1 OFFSET :index", nativeQuery = true)
    TaskDTO findOneByIndex(@NotNull @Param("userId") String userId, @NotNull @Param("index") Integer index);

    @NotNull
    @Query("SELECT t FROM TaskDTO t INNER JOIN FETCH UserDTO u ON t.userId = u.id WHERE u.id IN :users")
    List<TaskDTO> findAllByUsers(@NotNull @Param("users") Collection<String> collection);

    @NotNull
    @Query("SELECT t FROM TaskDTO t INNER JOIN FETCH ProjectDTO p ON t.projectId = p.id")
    List<TaskDTO> findAllByProjects();

    @NotNull
    @Query("SELECT t FROM TaskDTO t INNER JOIN FETCH ProjectDTO p ON t.projectId = p.id WHERE p.userId = :userId")
    List<TaskDTO> findAllByProjects(@NotNull @Param("userId") String userId);

    @NotNull
    @Query("SELECT t FROM TaskDTO t INNER JOIN FETCH ProjectDTO p ON t.projectId = p.id WHERE p.id IN :projects")
    List<TaskDTO> findAllByProjects(@NotNull @Param("projects") Collection<String> collection);

    @NotNull
    List<TaskDTO> findByUserIdAndProjectId(@NotNull @Param("userId") String userId, @NotNull @Param("projectId") String projectId);

    @NotNull
    List<TaskDTO> findByProjectId(@NotNull @Param("projectId") String projectId);

}
