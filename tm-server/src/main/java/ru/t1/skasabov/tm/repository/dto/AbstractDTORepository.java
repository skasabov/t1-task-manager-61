package ru.t1.skasabov.tm.repository.dto;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import ru.t1.skasabov.tm.dto.model.AbstractModelDTO;

@NoRepositoryBean
public interface AbstractDTORepository<M extends AbstractModelDTO> extends JpaRepository<M, String> {

}
