package ru.t1.skasabov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.skasabov.tm.dto.model.AbstractUserOwnedModelDTO;

import java.util.List;

@NoRepositoryBean
public interface AbstractUserOwnedDTORepository<M extends AbstractUserOwnedModelDTO> extends AbstractDTORepository<M> {

    @NotNull
    Boolean existsByUserIdAndId(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @NotNull
    List<M> findByUserId(@NotNull @Param("userId") String userId);

    @Nullable
    M findByUserIdAndId(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    long countByUserId(@NotNull @Param("userId") String userId);

    @Transactional
    void deleteByUserIdAndId(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Transactional
    void deleteByUserId(@NotNull @Param("userId") String userId);

}
