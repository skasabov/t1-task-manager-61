package ru.t1.skasabov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.skasabov.tm.model.Project;

import java.util.List;

@Repository
public interface ProjectRepository extends AbstractUserOwnedRepository<Project> {

    @Nullable
    @Query(value = "SELECT * FROM tm_project WHERE user_id = :userId LIMIT 1 OFFSET :index", nativeQuery = true)
    Project findOneByIndex(@NotNull @Param("userId") String userId, @NotNull @Param("index") Integer index);

    @NotNull
    @Query("SELECT p FROM Project p WHERE p.user.id = :userId ORDER BY p.created")
    List<Project> findAllSortByCreatedForUser(@NotNull @Param("userId") String userId);

    @NotNull
    @Query("SELECT p FROM Project p WHERE p.user.id = :userId ORDER BY p.status")
    List<Project> findAllSortByStatusForUser(@NotNull @Param("userId") String userId);

    @NotNull
    @Query("SELECT p FROM Project p WHERE p.user.id = :userId ORDER BY p.name")
    List<Project> findAllSortByNameForUser(@NotNull @Param("userId") String userId);

}
