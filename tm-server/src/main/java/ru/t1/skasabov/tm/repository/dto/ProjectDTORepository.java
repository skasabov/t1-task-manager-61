package ru.t1.skasabov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.skasabov.tm.dto.model.ProjectDTO;

import java.util.Collection;
import java.util.List;

@Repository
public interface ProjectDTORepository extends AbstractUserOwnedDTORepository<ProjectDTO> {

    @Nullable
    @Query(value = "SELECT * FROM tm_project WHERE user_id = :userId LIMIT 1 OFFSET :index", nativeQuery = true)
    ProjectDTO findOneByIndex(@NotNull @Param("userId") String userId, @NotNull @Param("index") Integer index);

    @NotNull
    @Query("SELECT p FROM ProjectDTO p INNER JOIN FETCH UserDTO u ON p.userId = u.id WHERE u.id IN :users")
    List<ProjectDTO> findAllByUsers(@NotNull @Param("users") Collection<String> collection);

    @NotNull
    @Query("SELECT p FROM ProjectDTO p WHERE p.userId = :userId ORDER BY p.created")
    List<ProjectDTO> findAllSortByCreatedForUser(@NotNull @Param("userId") String userId);

    @NotNull
    @Query("SELECT p FROM ProjectDTO p WHERE p.userId = :userId ORDER BY p.status")
    List<ProjectDTO> findAllSortByStatusForUser(@NotNull @Param("userId") String userId);

    @NotNull
    @Query("SELECT p FROM ProjectDTO p WHERE p.userId = :userId ORDER BY p.name")
    List<ProjectDTO> findAllSortByNameForUser(@NotNull @Param("userId") String userId);

}
