package ru.t1.skasabov.tm.taskmanager.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.skasabov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.skasabov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.skasabov.tm.api.endpoint.IUserEndpoint;
import ru.t1.skasabov.tm.dto.request.*;
import ru.t1.skasabov.tm.dto.response.*;
import ru.t1.skasabov.tm.enumerated.Role;
import ru.t1.skasabov.tm.enumerated.Sort;
import ru.t1.skasabov.tm.enumerated.Status;
import ru.t1.skasabov.tm.exception.field.SortIncorrectException;
import ru.t1.skasabov.tm.exception.field.StatusEmptyException;
import ru.t1.skasabov.tm.exception.field.StatusIncorrectException;
import ru.t1.skasabov.tm.dto.model.ProjectDTO;
import ru.t1.skasabov.tm.taskmanager.marker.SoapCategory;

import javax.xml.ws.WebServiceException;
import java.util.List;

public class ProjectEndpointTest extends AbstractEndpointTest {

    @NotNull
    private final IAuthEndpoint authEndpoint = context.getBean(IAuthEndpoint.class);

    @NotNull
    private final IUserEndpoint userEndpoint = context.getBean(IUserEndpoint.class);

    @NotNull
    private final IProjectEndpoint projectEndpoint = context.getBean(IProjectEndpoint.class);

    @Nullable
    private String token;

    @Nullable
    private String adminToken;

    @NotNull
    private List<ProjectDTO> projectList;

    @Nullable
    private ProjectDTO projectOne;

    @Nullable
    private ProjectDTO projectTwo;

    @Before
    public void initTest() {
        userEndpoint.registryUser(
                new UserRegistryRequest("123", "123", "123@123")
        );
        @NotNull final UserLoginResponse response = authEndpoint.login(
                new UserLoginRequest("123", "123")
        );
        token = response.getToken();
        userEndpoint.registryUserRole(
                new UserRoleRegistryRequest("456", "456", Role.ADMIN)
        );
        @NotNull final UserLoginResponse adminResponse = authEndpoint.login(
                new UserLoginRequest("456", "456")
        );
        adminToken = adminResponse.getToken();
        @NotNull final ProjectCreateRequest requestOne = new ProjectCreateRequest(
                "project_one", "project_one"
        );
        requestOne.setToken(token);
        @NotNull final ProjectCreateRequest requestTwo = new ProjectCreateRequest(
                "project_two", "project_two"
        );
        requestTwo.setToken(token);
        @NotNull final ProjectCreateResponse responseTwo = projectEndpoint.createProject(requestTwo);
        @NotNull final ProjectCreateResponse responseOne = projectEndpoint.createProject(requestOne);
        projectOne = responseOne.getProject();
        projectTwo = responseTwo.getProject();
        @NotNull final ProjectListRequest projectListRequest = new ProjectListRequest();
        projectListRequest.setToken(token);
        projectList = projectEndpoint.listProjects(projectListRequest).getProjects();
    }

    @Test
    @Category(SoapCategory.class)
    public void testChangeProjectStatusById() {
        Assert.assertNotNull(projectOne);
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(
                projectOne.getId(), Status.NOT_STARTED
        );
        request.setToken(token);
        @NotNull final ProjectChangeStatusByIdResponse response = projectEndpoint.changeProjectStatusById(request);
        Assert.assertNotNull(response.getProject());
        Assert.assertEquals(Status.NOT_STARTED, response.getProject().getStatus());
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testChangeProjectStatusByEmptyId() {
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(
                "", Status.NOT_STARTED
        );
        request.setToken(token);
        projectEndpoint.changeProjectStatusById(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testChangeProjectStatusByNullId() {
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(
                "", Status.NOT_STARTED
        );
        request.setToken(token);
        projectEndpoint.changeProjectStatusById(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testChangeProjectStatusByInvalidId() {
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(
                "???", Status.NOT_STARTED
        );
        request.setToken(token);
        projectEndpoint.changeProjectStatusById(request);
    }

    @Test(expected = StatusEmptyException.class)
    @Category(SoapCategory.class)
    public void testChangeProjectEmptyStatusById() {
        Assert.assertNotNull(projectOne);
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(
                projectOne.getId(), Status.toStatus(null)
        );
        request.setToken(token);
        projectEndpoint.changeProjectStatusById(request);
    }

    @Test(expected = StatusIncorrectException.class)
    @Category(SoapCategory.class)
    public void testChangeProjectInvalidStatusById() {
        Assert.assertNotNull(projectOne);
        new ProjectChangeStatusByIdRequest(projectOne.getId(), Status.toStatus("???"));
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testChangeProjectStatusByIdNoAuth() {
        Assert.assertNotNull(projectOne);
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(
                projectOne.getId(), Status.NOT_STARTED
        );
        projectEndpoint.changeProjectStatusById(request);
    }

    @Test
    @Category(SoapCategory.class)
    public void testChangeProjectStatusByIndex() {
        @NotNull final ProjectChangeStatusByIndexRequest request = new ProjectChangeStatusByIndexRequest(
                0, Status.COMPLETED
        );
        request.setToken(token);
        @NotNull final ProjectChangeStatusByIndexResponse response = projectEndpoint.changeProjectStatusByIndex(request);
        Assert.assertNotNull(response.getProject());
        Assert.assertEquals(Status.COMPLETED, response.getProject().getStatus());
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testChangeProjectStatusByEmptyIndex() {
        @NotNull final ProjectChangeStatusByIndexRequest request = new ProjectChangeStatusByIndexRequest(
                null, Status.NOT_STARTED
        );
        request.setToken(token);
        projectEndpoint.changeProjectStatusByIndex(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testChangeProjectStatusByNegativeIndex() {
        @NotNull final ProjectChangeStatusByIndexRequest request = new ProjectChangeStatusByIndexRequest(
                -2, Status.NOT_STARTED
        );
        request.setToken(token);
        projectEndpoint.changeProjectStatusByIndex(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testChangeProjectStatusByInvalidIndex() {
        @NotNull final ProjectChangeStatusByIndexRequest request = new ProjectChangeStatusByIndexRequest(
                2, Status.NOT_STARTED
        );
        request.setToken(token);
        projectEndpoint.changeProjectStatusByIndex(request);
    }

    @Test(expected = StatusEmptyException.class)
    @Category(SoapCategory.class)
    public void testChangeProjectEmptyStatusByIndex() {
        @NotNull final ProjectChangeStatusByIndexRequest request = new ProjectChangeStatusByIndexRequest(
                0, Status.toStatus(null)
        );
        request.setToken(token);
        projectEndpoint.changeProjectStatusByIndex(request);
    }

    @Test(expected = StatusIncorrectException.class)
    @Category(SoapCategory.class)
    public void testChangeProjectInvalidStatusByIndex() {
        new ProjectChangeStatusByIndexRequest(0, Status.toStatus("???"));
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testChangeProjectStatusByIndexNoAuth() {
        @NotNull final ProjectChangeStatusByIndexRequest request = new ProjectChangeStatusByIndexRequest(
                0, Status.NOT_STARTED
        );
        projectEndpoint.changeProjectStatusByIndex(request);
    }

    @Test
    @Category(SoapCategory.class)
    public void testClearProjects() {
        @NotNull final ProjectClearRequest request = new ProjectClearRequest();
        request.setToken(token);
        @NotNull final ProjectClearResponse response = projectEndpoint.clearProjects(request);
        Assert.assertEquals(0, response.getProjects().size());
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testClearProjectsNoAuth() {
        @NotNull final ProjectClearRequest request = new ProjectClearRequest();
        projectEndpoint.clearProjects(request);
    }

    @Test
    @Category(SoapCategory.class)
    public void testCreateProject() {
        @NotNull final ProjectCreateRequest request = new ProjectCreateRequest(
                "project_three", "project_three"
        );
        request.setToken(token);
        @NotNull final ProjectCreateResponse response = projectEndpoint.createProject(request);
        Assert.assertNotNull(response.getProject());
        Assert.assertEquals("project_three", response.getProject().getName());
        Assert.assertEquals("project_three", response.getProject().getDescription());
    }

    @Test
    @Category(SoapCategory.class)
    public void testCreateProjectWithName() {
        @NotNull final ProjectCreateRequest request = new ProjectCreateRequest("project_three", "");
        request.setToken(token);
        @NotNull final ProjectCreateResponse response = projectEndpoint.createProject(request);
        Assert.assertNotNull(response.getProject());
        Assert.assertEquals("project_three", response.getProject().getName());
        Assert.assertEquals("", response.getProject().getDescription());
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testCreateProjectWithEmptyName() {
        @NotNull final ProjectCreateRequest request = new ProjectCreateRequest("", "project_three");
        request.setToken(token);
        projectEndpoint.createProject(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testCreateProjectWithNullName() {
        @NotNull final ProjectCreateRequest request = new ProjectCreateRequest(null, "project_three");
        request.setToken(token);
        projectEndpoint.createProject(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testCreateProjectNoAuth() {
        @NotNull final ProjectCreateRequest request = new ProjectCreateRequest(
                "project_three", "project_three"
        );
        projectEndpoint.createProject(request);
    }

    @Test
    @Category(SoapCategory.class)
    public void testListProjects() {
        @NotNull final ProjectListRequest request = new ProjectListRequest();
        request.setToken(token);
        @NotNull final ProjectListResponse response = projectEndpoint.listProjects(request);
        Assert.assertEquals(2, response.getProjects().size());
    }

    @Test
    @Category(SoapCategory.class)
    public void testListProjectsWithSort() {
        @NotNull final ProjectListRequest request = new ProjectListRequest(Sort.BY_NAME);
        request.setToken(token);
        @NotNull final ProjectListResponse response = projectEndpoint.listProjects(request);
        Assert.assertEquals("project_one", response.getProjects().get(0).getName());
        Assert.assertEquals("project_two", response.getProjects().get(1).getName());
    }

    @Test
    @Category(SoapCategory.class)
    public void testListProjectsWithNullSort() {
        @NotNull final ProjectListRequest request = new ProjectListRequest(null);
        request.setToken(token);
        @NotNull final ProjectListResponse response = projectEndpoint.listProjects(request);
        Assert.assertEquals(2, response.getProjects().size());
    }

    @Test(expected = SortIncorrectException.class)
    @Category(SoapCategory.class)
    public void testListProjectsWithInvalidSort() {
        new ProjectListRequest(Sort.toSort("???"));
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testListProjectsNoAuth() {
        @NotNull final ProjectListRequest request = new ProjectListRequest();
        projectEndpoint.listProjects(request);
    }

    @Test
    @Category(SoapCategory.class)
    public void testGetProjectById() {
        Assert.assertNotNull(projectOne);
        @NotNull final ProjectGetByIdRequest request = new ProjectGetByIdRequest(projectOne.getId());
        request.setToken(token);
        @NotNull final ProjectGetByIdResponse response = projectEndpoint.getProjectById(request);
        Assert.assertNotNull(response.getProject());
        Assert.assertEquals(projectOne.getName(), response.getProject().getName());
        Assert.assertEquals(projectOne.getDescription(), response.getProject().getDescription());
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testGetProjectByEmptyId() {
        @NotNull final ProjectGetByIdRequest request = new ProjectGetByIdRequest("");
        request.setToken(token);
        projectEndpoint.getProjectById(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testGetProjectByNullId() {
        @NotNull final ProjectGetByIdRequest request = new ProjectGetByIdRequest(null);
        request.setToken(token);
        projectEndpoint.getProjectById(request);
    }

    @Test
    @Category(SoapCategory.class)
    public void testGetProjectByInvalidId() {
        @NotNull final ProjectGetByIdRequest request = new ProjectGetByIdRequest("???");
        request.setToken(token);
        @NotNull final ProjectGetByIdResponse response = projectEndpoint.getProjectById(request);
        Assert.assertNull(response.getProject());
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testGetProjectByIdNoAuth() {
        Assert.assertNotNull(projectOne);
        @NotNull final ProjectGetByIdRequest request = new ProjectGetByIdRequest(projectOne.getId());
        projectEndpoint.getProjectById(request);
    }

    @Test
    @Category(SoapCategory.class)
    public void testGetProjectByIndex() {
        @NotNull final ProjectDTO project = projectList.get(0);
        @NotNull final ProjectGetByIndexRequest request = new ProjectGetByIndexRequest(0);
        request.setToken(token);
        @NotNull final ProjectGetByIndexResponse response = projectEndpoint.getProjectByIndex(request);
        Assert.assertNotNull(response.getProject());
        Assert.assertEquals(project.getName(), response.getProject().getName());
        Assert.assertEquals(project.getDescription(), response.getProject().getDescription());
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testGetProjectByEmptyIndex() {
        @NotNull final ProjectGetByIndexRequest request = new ProjectGetByIndexRequest(null);
        request.setToken(token);
        projectEndpoint.getProjectByIndex(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testGetProjectByNegativeIndex() {
        @NotNull final ProjectGetByIndexRequest request = new ProjectGetByIndexRequest(-2);
        request.setToken(token);
        projectEndpoint.getProjectByIndex(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testGetProjectByInvalidIndex() {
        @NotNull final ProjectGetByIndexRequest request = new ProjectGetByIndexRequest(2);
        request.setToken(token);
        projectEndpoint.getProjectByIndex(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testGetProjectByIndexNoAuth() {
        @NotNull final ProjectGetByIndexRequest request = new ProjectGetByIndexRequest(0);
        projectEndpoint.getProjectByIndex(request);
    }

    @Test
    @Category(SoapCategory.class)
    public void testRemoveProjectById() {
        Assert.assertNotNull(projectOne);
        @NotNull final ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(projectOne.getId());
        request.setToken(token);
        @NotNull final ProjectRemoveByIdResponse response = projectEndpoint.removeProjectById(request);
        Assert.assertNotNull(response.getProject());
        Assert.assertEquals(projectOne.getName(), response.getProject().getName());
        Assert.assertEquals(projectOne.getDescription(), response.getProject().getDescription());
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testRemoveProjectByEmptyId() {
        @NotNull final ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest("");
        request.setToken(token);
        projectEndpoint.removeProjectById(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testRemoveProjectByNullId() {
        @NotNull final ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(null);
        request.setToken(token);
        projectEndpoint.removeProjectById(request);
    }

    @Test
    @Category(SoapCategory.class)
    public void testRemoveProjectByInvalidId() {
        @NotNull final ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest("???");
        request.setToken(token);
        @NotNull final ProjectRemoveByIdResponse response = projectEndpoint.removeProjectById(request);
        Assert.assertNull(response.getProject());
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testRemoveProjectByIdNoAuth() {
        Assert.assertNotNull(projectOne);
        @NotNull final ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(projectOne.getId());
        projectEndpoint.removeProjectById(request);
    }

    @Test
    @Category(SoapCategory.class)
    public void testRemoveProjectByIndex() {
        @NotNull final ProjectDTO project = projectList.get(0);
        @NotNull final ProjectRemoveByIndexRequest request = new ProjectRemoveByIndexRequest(0);
        request.setToken(token);
        @NotNull final ProjectRemoveByIndexResponse response = projectEndpoint.removeProjectByIndex(request);
        Assert.assertNotNull(response.getProject());
        Assert.assertEquals(project.getName(), response.getProject().getName());
        Assert.assertEquals(project.getDescription(), response.getProject().getDescription());
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testRemoveProjectByEmptyIndex() {
        @NotNull final ProjectRemoveByIndexRequest request = new ProjectRemoveByIndexRequest(null);
        request.setToken(token);
        projectEndpoint.removeProjectByIndex(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testRemoveProjectByNegativeIndex() {
        @NotNull final ProjectRemoveByIndexRequest request = new ProjectRemoveByIndexRequest(-2);
        request.setToken(token);
        projectEndpoint.removeProjectByIndex(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testRemoveProjectByInvalidIndex() {
        @NotNull final ProjectRemoveByIndexRequest request = new ProjectRemoveByIndexRequest(2);
        request.setToken(token);
        projectEndpoint.removeProjectByIndex(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testRemoveProjectByIndexNoAuth() {
        @NotNull final ProjectRemoveByIndexRequest request = new ProjectRemoveByIndexRequest(0);
        projectEndpoint.removeProjectByIndex(request);
    }

    @Test
    @Category(SoapCategory.class)
    public void testUpdateProjectById() {
        Assert.assertNotNull(projectOne);
        @NotNull final ProjectUpdateByIdRequest request = new ProjectUpdateByIdRequest(
                projectOne.getId(), "project_three", "project_three"
        );
        request.setToken(token);
        @NotNull final ProjectUpdateByIdResponse response = projectEndpoint.updateProjectById(request);
        Assert.assertNotNull(response.getProject());
        Assert.assertEquals("project_three", response.getProject().getName());
        Assert.assertEquals("project_three", response.getProject().getDescription());
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testUpdateProjectByEmptyId() {
        @NotNull final ProjectUpdateByIdRequest request = new ProjectUpdateByIdRequest(
                "", "project_three", "project_three"
        );
        request.setToken(token);
        projectEndpoint.updateProjectById(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testUpdateProjectByNullId() {
        @NotNull final ProjectUpdateByIdRequest request = new ProjectUpdateByIdRequest(
                null, "project_three", "project_three"
        );
        request.setToken(token);
        projectEndpoint.updateProjectById(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testUpdateProjectByInvalidId() {
        @NotNull final ProjectUpdateByIdRequest request = new ProjectUpdateByIdRequest(
                "???", "project_three", "project_three"
        );
        request.setToken(token);
        projectEndpoint.updateProjectById(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testUpdateProjectByIdWithEmptyName() {
        Assert.assertNotNull(projectOne);
        @NotNull final ProjectUpdateByIdRequest request = new ProjectUpdateByIdRequest(
                projectOne.getId(), "", "project_three"
        );
        request.setToken(token);
        projectEndpoint.updateProjectById(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testUpdateProjectByIdWithNullName() {
        Assert.assertNotNull(projectOne);
        @NotNull final ProjectUpdateByIdRequest request = new ProjectUpdateByIdRequest(
                projectOne.getId(), null, "project_three"
        );
        request.setToken(token);
        projectEndpoint.updateProjectById(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testUpdateProjectByIdNoAuth() {
        Assert.assertNotNull(projectOne);
        @NotNull final ProjectUpdateByIdRequest request = new ProjectUpdateByIdRequest(
                projectOne.getId(), "project_three", "project_three"
        );
        projectEndpoint.updateProjectById(request);
    }

    @Test
    @Category(SoapCategory.class)
    public void testUpdateProjectByIndex() {
        @NotNull final ProjectUpdateByIndexRequest request = new ProjectUpdateByIndexRequest(
                0, "project_three", "project_three"
        );
        request.setToken(token);
        @NotNull final ProjectUpdateByIndexResponse response = projectEndpoint.updateProjectByIndex(request);
        Assert.assertNotNull(response.getProject());
        Assert.assertEquals("project_three", response.getProject().getName());
        Assert.assertEquals("project_three", response.getProject().getDescription());
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testUpdateProjectByEmptyIndex() {
        @NotNull final ProjectUpdateByIndexRequest request = new ProjectUpdateByIndexRequest(
                null, "project_three", "project_three"
        );
        request.setToken(token);
        projectEndpoint.updateProjectByIndex(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testUpdateProjectByNegativeIndex() {
        @NotNull final ProjectUpdateByIndexRequest request = new ProjectUpdateByIndexRequest(
                -2, "project_three", "project_three"
        );
        request.setToken(token);
        projectEndpoint.updateProjectByIndex(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testUpdateProjectByInvalidIndex() {
        @NotNull final ProjectUpdateByIndexRequest request = new ProjectUpdateByIndexRequest(
                2, "project_three", "project_three"
        );
        request.setToken(token);
        projectEndpoint.updateProjectByIndex(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testUpdateProjectByIndexWithEmptyName() {
        @NotNull final ProjectUpdateByIndexRequest request = new ProjectUpdateByIndexRequest(
                0, "", "project_three"
        );
        request.setToken(token);
        projectEndpoint.updateProjectByIndex(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testUpdateProjectByIndexWithNullName() {
        @NotNull final ProjectUpdateByIndexRequest request = new ProjectUpdateByIndexRequest(
                0, null, "project_three"
        );
        request.setToken(token);
        projectEndpoint.updateProjectByIndex(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testUpdateProjectByIndexNoAuth() {
        @NotNull final ProjectUpdateByIndexRequest request = new ProjectUpdateByIndexRequest(
                0, "project_three", "project_three"
        );
        projectEndpoint.updateProjectByIndex(request);
    }

    @Test
    @Category(SoapCategory.class)
    public void testStartProjectById() {
        Assert.assertNotNull(projectOne);
        @NotNull final ProjectStartByIdRequest request = new ProjectStartByIdRequest(projectOne.getId());
        request.setToken(token);
        @NotNull final ProjectStartByIdResponse response = projectEndpoint.startProjectById(request);
        Assert.assertNotNull(response.getProject());
        Assert.assertEquals(Status.IN_PROGRESS, response.getProject().getStatus());
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testStartProjectByEmptyId() {
        @NotNull final ProjectStartByIdRequest request = new ProjectStartByIdRequest("");
        request.setToken(token);
        projectEndpoint.startProjectById(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testStartProjectByNullId() {
        @NotNull final ProjectStartByIdRequest request = new ProjectStartByIdRequest(null);
        request.setToken(token);
        projectEndpoint.startProjectById(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testStartProjectByInvalidId() {
        @NotNull final ProjectStartByIdRequest request = new ProjectStartByIdRequest("???");
        request.setToken(token);
        projectEndpoint.startProjectById(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testStartProjectByIdNoAuth() {
        Assert.assertNotNull(projectOne);
        @NotNull final ProjectStartByIdRequest request = new ProjectStartByIdRequest(projectOne.getId());
        projectEndpoint.startProjectById(request);
    }

    @Test
    @Category(SoapCategory.class)
    public void testStartProjectByIndex() {
        @NotNull final ProjectStartByIndexRequest request = new ProjectStartByIndexRequest(0);
        request.setToken(token);
        @NotNull final ProjectStartByIndexResponse response = projectEndpoint.startProjectByIndex(request);
        Assert.assertNotNull(response.getProject());
        Assert.assertEquals(Status.IN_PROGRESS, response.getProject().getStatus());
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testStartProjectByEmptyIndex() {
        @NotNull final ProjectStartByIndexRequest request = new ProjectStartByIndexRequest(null);
        request.setToken(token);
        projectEndpoint.startProjectByIndex(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testStartProjectByNegativeIndex() {
        @NotNull final ProjectStartByIndexRequest request = new ProjectStartByIndexRequest(-2);
        request.setToken(token);
        projectEndpoint.startProjectByIndex(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testStartProjectByInvalidIndex() {
        @NotNull final ProjectStartByIndexRequest request = new ProjectStartByIndexRequest(2);
        request.setToken(token);
        projectEndpoint.startProjectByIndex(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testStartProjectByIndexNoAuth() {
        @NotNull final ProjectStartByIndexRequest request = new ProjectStartByIndexRequest(0);
        projectEndpoint.startProjectByIndex(request);
    }

    @Test
    @Category(SoapCategory.class)
    public void testCompleteProjectById() {
        Assert.assertNotNull(projectOne);
        @NotNull final ProjectCompleteByIdRequest request = new ProjectCompleteByIdRequest(projectOne.getId());
        request.setToken(token);
        @NotNull final ProjectCompleteByIdResponse response = projectEndpoint.completeProjectById(request);
        Assert.assertNotNull(response.getProject());
        Assert.assertEquals(Status.COMPLETED, response.getProject().getStatus());
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testCompleteProjectByEmptyId() {
        @NotNull final ProjectCompleteByIdRequest request = new ProjectCompleteByIdRequest("");
        request.setToken(token);
        projectEndpoint.completeProjectById(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testCompleteProjectByNullId() {
        @NotNull final ProjectCompleteByIdRequest request = new ProjectCompleteByIdRequest(null);
        request.setToken(token);
        projectEndpoint.completeProjectById(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testCompleteProjectByInvalidId() {
        @NotNull final ProjectCompleteByIdRequest request = new ProjectCompleteByIdRequest("???");
        request.setToken(token);
        projectEndpoint.completeProjectById(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testCompleteProjectByIdNoAuth() {
        Assert.assertNotNull(projectOne);
        @NotNull final ProjectCompleteByIdRequest request = new ProjectCompleteByIdRequest(projectOne.getId());
        projectEndpoint.completeProjectById(request);
    }

    @Test
    @Category(SoapCategory.class)
    public void testCompleteProjectByIndex() {
        @NotNull final ProjectCompleteByIndexRequest request = new ProjectCompleteByIndexRequest(0);
        request.setToken(token);
        @NotNull final ProjectCompleteByIndexResponse response = projectEndpoint.completeProjectByIndex(request);
        Assert.assertNotNull(response.getProject());
        Assert.assertEquals(Status.COMPLETED, response.getProject().getStatus());
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testCompleteProjectByEmptyIndex() {
        @NotNull final ProjectCompleteByIndexRequest request = new ProjectCompleteByIndexRequest(null);
        request.setToken(token);
        projectEndpoint.completeProjectByIndex(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testCompleteProjectByNegativeIndex() {
        @NotNull final ProjectCompleteByIndexRequest request = new ProjectCompleteByIndexRequest(-2);
        request.setToken(token);
        projectEndpoint.completeProjectByIndex(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testCompleteProjectByInvalidIndex() {
        @NotNull final ProjectCompleteByIndexRequest request = new ProjectCompleteByIndexRequest(2);
        request.setToken(token);
        projectEndpoint.completeProjectByIndex(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testCompleteProjectByIndexNoAuth() {
        @NotNull final ProjectCompleteByIndexRequest request = new ProjectCompleteByIndexRequest(0);
        projectEndpoint.completeProjectByIndex(request);
    }

    @After
    public void endTest() {
        Assert.assertNotNull(projectOne);
        Assert.assertNotNull(projectTwo);
        @NotNull final ProjectRemoveByIdRequest requestOne = new ProjectRemoveByIdRequest(
                projectOne.getId()
        );
        @NotNull final ProjectRemoveByIdRequest requestTwo = new ProjectRemoveByIdRequest(
                projectTwo.getId()
        );
        @NotNull final UserLogoutRequest userLogoutRequest = new UserLogoutRequest();
        requestOne.setToken(token);
        requestTwo.setToken(token);
        userLogoutRequest.setToken(token);
        projectEndpoint.removeProjectById(requestOne);
        projectEndpoint.removeProjectById(requestTwo);
        authEndpoint.logout(userLogoutRequest);
        @NotNull final UserRemoveRequest removeRequest = new UserRemoveRequest("123");
        removeRequest.setToken(adminToken);
        userEndpoint.removeUser(removeRequest);
        @NotNull final UserLogoutRequest adminLogoutRequest = new UserLogoutRequest();
        adminLogoutRequest.setToken(adminToken);
        authEndpoint.logout(adminLogoutRequest);
        @NotNull final UserRemoveRequest adminRemoveRequest = new UserRemoveRequest("456");
        adminRemoveRequest.setToken(adminToken);
        userEndpoint.removeUser(adminRemoveRequest);
    }

}
