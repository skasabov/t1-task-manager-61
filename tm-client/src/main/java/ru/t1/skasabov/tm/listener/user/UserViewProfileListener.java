package ru.t1.skasabov.tm.listener.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.skasabov.tm.dto.request.UserProfileRequest;
import ru.t1.skasabov.tm.dto.response.UserProfileResponse;
import ru.t1.skasabov.tm.event.ConsoleEvent;

@Component
@NoArgsConstructor
public final class UserViewProfileListener extends AbstractUserListener {

    @NotNull
    private static final String NAME = "view-user-profile";

    @NotNull
    private static final String DESCRIPTION = "View profile of current user.";

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String command() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@userViewProfileListener.command() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[USER VIEW PROFILE]");
        @NotNull final UserProfileRequest request = new UserProfileRequest();
        request.setToken(getToken());
        @NotNull final UserProfileResponse response = authEndpoint.profile(request);
        showUser(response.getUser());
    }

}
