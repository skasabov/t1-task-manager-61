package ru.t1.skasabov.tm.listener.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.skasabov.tm.dto.request.ProjectListRequest;
import ru.t1.skasabov.tm.dto.response.ProjectListResponse;
import ru.t1.skasabov.tm.enumerated.Sort;
import ru.t1.skasabov.tm.event.ConsoleEvent;
import ru.t1.skasabov.tm.util.TerminalUtil;

import java.util.Arrays;

@Component
@NoArgsConstructor
public final class ProjectListListener extends AbstractProjectListener {

    @NotNull
    private static final String NAME = "project-list";

    @NotNull
    private static final String DESCRIPTION = "Show all projects.";

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String command() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@projectListListener.command() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @Nullable final Sort sort = Sort.toSort(sortType);
        @NotNull final ProjectListRequest request = new ProjectListRequest(sort);
        request.setToken(getToken());
        @NotNull final ProjectListResponse response = projectEndpoint.listProjects(request);
        renderProjects(response.getProjects());
    }

}
