package ru.t1.skasabov.tm.listener.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.skasabov.tm.dto.request.UserLockRequest;
import ru.t1.skasabov.tm.event.ConsoleEvent;
import ru.t1.skasabov.tm.util.TerminalUtil;

@Component
@NoArgsConstructor
public final class UserLockListener extends AbstractUserListener {

    @NotNull
    private static final String NAME = "user-lock";

    @NotNull
    private static final String DESCRIPTION = "User lock.";

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String command() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@userLockListener.command() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[USER LOCK]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        @NotNull final UserLockRequest request = new UserLockRequest(login);
        request.setToken(getToken());
        userEndpoint.lockUser(request);
    }

}
