package ru.t1.skasabov.tm.listener.system;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.skasabov.tm.listener.AbstractListener;
import ru.t1.skasabov.tm.event.ConsoleEvent;

@Component
@NoArgsConstructor
public final class ApplicationCommandListListener extends AbstractSystemListener {

    @NotNull
    private static final String NAME = "commands";

    @NotNull
    private static final String DESCRIPTION = "Show command list.";

    @NotNull
    private static final String ARGUMENT = "-cmd";

    @NotNull
    @Override
    public String argument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String command() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@applicationCommandListListener.command() == #consoleEvent.name " +
            "or @applicationCommandListListener.argument() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[COMMANDS]");
        for (@NotNull final AbstractListener listener : listeners) {
            @Nullable final String name = listener.command();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

}
