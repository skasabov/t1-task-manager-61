package ru.t1.skasabov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.skasabov.tm.dto.request.DataXmlLoadFasterXmlRequest;
import ru.t1.skasabov.tm.event.ConsoleEvent;

@Component
public final class DataXmlLoadFasterXmlListener extends AbstractDataListener {

    @NotNull
    private static final String NAME = "data-load-xml";

    @NotNull
    private static final String DESCRIPTION = "Load data from xml file.";

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String command() {
        return NAME;
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataXmlLoadFasterXmlListener.command() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[DATA LOAD XML]");
        @NotNull final DataXmlLoadFasterXmlRequest request = new DataXmlLoadFasterXmlRequest();
        request.setToken(getToken());
        domainEndpoint.loadDataXmlFasterXml(request);
    }

}
